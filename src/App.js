import "antd/dist/antd.css";
import "bootstrap/dist/css/bootstrap.min.css";
import React, { Suspense } from "react";
//import AppRouter from "./app/config/routes.js";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import "./App.css";
import Layout from "./app/component/layout";
import { ROUTER_CONST } from "./app/config/const";
import AppRouter from "./app/config/routes";
import DetailNewsPage from "./app/page/detailNews";
import NotFoundPage from "./app/page/error";
import HomePage from "./app/page/home";
import LoginScreen from "./app/page/loginScreen";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";
import AppRouterAdministrative from "./app/config/routesAdministrative";
import AppRouterPolice from "./app/config/routesPolice";
function App() {
  const loader = <div className="example">Loading...</div>;
  return (
    //  const { collapsed } = this.state;
    <>
      <ToastContainer autoClose={3000} theme="colored" position="top-center" />
      <BrowserRouter>
        <Suspense fallback={loader}>
          <Switch>
            <Route path={ROUTER_CONST.home} component={HomePage} exact />
            <Route
              path={ROUTER_CONST.newDetail}
              render={(props) => <DetailNewsPage {...props} />}
            />
            <Route path={ROUTER_CONST.login} component={LoginScreen} exact />
            <Route path={ROUTER_CONST.admin} component={AppRouter} />
            <Route
              path={ROUTER_CONST.administrative}
              component={AppRouterAdministrative}
            />
            <Route path={ROUTER_CONST.police} component={AppRouterPolice} />
            <Route path="*" component={NotFoundPage} />
          </Switch>
        </Suspense>
      </BrowserRouter>
    </>
  );
}

export default App;
