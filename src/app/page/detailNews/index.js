import React from "react";
import DetailNewsComponent from "../../component/pageComponent/detailNews/DetailNews";

function DetailNewsPage(props) {
  return <DetailNewsComponent {...props} />;
}

export default DetailNewsPage;
