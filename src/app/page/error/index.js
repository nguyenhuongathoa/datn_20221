import React from 'react'

function NotFoundPage() {
    return (
        <div className="text-center">
            <h2>404 page not found</h2>
        </div>
    )
}

export default NotFoundPage
