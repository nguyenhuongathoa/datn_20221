import React from "react";
import { Form, Input, Button, Checkbox, Card } from "antd";
import instance from "../../config/axiosClient";
import {
  CITIZEN_ID,
  FULL_NAME,
  ROLE,
  ROLE_TYPE,
  USER_ID,
  USER_NAME,
} from "../../config/const";
import { useHistory } from "react-router";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./style.css";
import { ROUTER_CONST } from "../../config/const";
//"./app/config/const";

export default function LoginScreen() {
  const history = useHistory();
  const onFinish = (values) => {
    instance
      .post("/v1/user/singin", values)
      .then((res) => {
        const user = res?.data?.data;
        localStorage.setItem(USER_NAME, user[USER_NAME]);
        localStorage.setItem(USER_ID, user[USER_ID]);
        localStorage.setItem(FULL_NAME, user[FULL_NAME]);
        localStorage.setItem(CITIZEN_ID, user[CITIZEN_ID]);
        localStorage.setItem(ROLE, user[ROLE]);
        localStorage.setItem(ROLE_TYPE, user[ROLE_TYPE]);

        if (user?.role === "ADMIN") {
          history.push(ROUTER_CONST.noticeManagement);
        } else if (user?.role === "USER_POLICE") {
          history.push(ROUTER_CONST.policeCitizenManagement);
        } else {
          history.push(ROUTER_CONST.administrativeNoticeManagement);
        }
      })
      .catch((err) => {
        const message = err?.response?.data?.message || "Đăng nhập thất bại!";
        toast.error(message);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="login-page">
      <Card
        style={{
          width: "50%",
          height: 300,
          borderRadius: 15,
          boxShadow: "0px 0px 10px 3px #cecece",
        }}
      >
        <Form
          style={{ marginTop: 35 }}
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Tên đăng nhập"
            name="userName"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập tên đăng nhập!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Mật khẩu"
            name="passWord"
            rules={[{ required: true, message: "Vui lòng nhập mật khẩu!" }]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            name="remember"
            valuePropName="checked"
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Checkbox>Remember me</Checkbox>
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
}
