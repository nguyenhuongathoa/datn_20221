import { Form, Input, Modal, Select } from "antd";
import React, { useEffect, useState } from "react";
import SelectCustome from "../../../helpers/SelectCustome";
import "./style.css";

export default function UserForm(props) {
  const handleOk = () => {
    const formValue = form.getFieldsValue(true);
    console.log(form);
    console.log(formValue);
    props.handleSubmit(formValue);
  };
  const handleCancel = () => {
    props.toggleForm();
  };
  const [form] = Form.useForm();
  const onFinish = (values) => {
    console.log(values);
  };
  const onFinishFailed = () => {};
  useEffect(() => {
    console.log("render");
    form.setFieldsValue({
      ...props.initialValues,
    });
  }, [props.initialValues]);
  return (
    <div className="user-form">
      <Modal
        title={"Thêm mới tài khoản"}
        visible={props.visible}
        onOk={handleOk}
        onCancel={handleCancel}
        width={1000}
      >
        <Form
          form={form}
          layout="vertical"
          name="basic"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <div className="row">
            <div className="col-6">
              {" "}
              <Form.Item label="Chủ hộ">
                <Form.Item
                  name="citizenId"
                  noStyle
                  rules={[
                    { required: true, message: "công dân là trường bắt buộc" },
                  ]}
                >
                  <Select
                    placeholder="chọn công dân"
                    options={props.citizenHosts}
                  ></Select>
                </Form.Item>
              </Form.Item>
            </div>
            <div className="col-6">
              <Form.Item
                label="Chức vụ"
                name="role"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng chọn phân loại!",
                  },
                ]}
              >
                <SelectCustome
                  placeholder="Chọn chức vụ"
                  options={props.roleCategories}
                />
              </Form.Item>
            </div>
          </div>
          <div className="row">
            <div className="col-6">
              {" "}
              <Form.Item
                label="Tên đăng nhập "
                name="userName"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập tên đăng nhập!",
                  },
                ]}
                initialValue=""
              >
                <Input />
              </Form.Item>
            </div>
            <div className="col-6">
              {" "}
              <Form.Item
                label="Mật khẩu "
                name="userPassword"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập mật khẩu!",
                  },
                ]}
                initialValue=""
              >
                <Input />
              </Form.Item>
            </div>
          </div>
        </Form>
      </Modal>
    </div>
  );
}
