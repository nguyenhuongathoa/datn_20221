import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Button, Card, Popconfirm, Spin, Table } from "antd";
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import API from "../../../config/api";
import instance from "../../../config/axiosClient";
import { GET_LIST_ERROR, USER_NAME } from "../../../config/const";
import { normalizeToOptions } from "../../../helpers/utlis";
import UserForm from "./form";
import "./style.css";

const pageSize = 50;
const defaultForm = {
  isShowForm: false,
  modalUD: {
    id: null,
    citizenId: null,
    userName: null,
    userPassword: null,
    role: null,
  },
};

export default function UserManagement() {
  const [users, setUsers] = useState([]);
  const [pagination, setPagination] = useState({
    pageSize: 50,
    pageIndex: 1,
    totalRecord: 0,
  });
  const getFullNameAndId = (info) =>
    `${info?.fullName} - ${info?.indentificationNumber}`;
  const [form, setForm] = useState(defaultForm);
  const [roleCategories, setRoleCategories] = useState([]);
  const [citizenHosts, setCitizenHosts] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [maxCounts, setMaxCounts] = useState([]);

  const getAndSetData = () => {
    setIsLoading(true);

    instance
      .get(API.user.getList)
      .then((res) => {
        const data = res?.data?.data?.object;
        const max = res?.data?.data?.maxCount;
        const paging = {
          totalRecord: res?.data?.data?.maxCount,
          pageIndex: res?.data?.data?.pageIndex,
          pageSize: res?.data?.data?.pageSize,
        };
        setUsers(data);
        setPagination(paging);
        setIsLoading(false);
        setMaxCounts(max);
      })
      .catch(() => {
        toast.error(GET_LIST_ERROR);
        setIsLoading(false);
      });
  };
  useEffect(() => {
    instance.get(`${API.reference.getCategory}ROLE_TYPE`).then((res) => {
      const options = res?.data?.data
        ? normalizeToOptions(res?.data?.data, "name", "code")
        : [];
      setRoleCategories(options);
    });
    instance.get(API.citizen.getForUser).then((res) => {
      const options = res?.data?.data
        ? normalizeToOptions(res?.data?.data, getFullNameAndId, "id")
        : [];
      setCitizenHosts(options);
    });
    getAndSetData();
  }, []);
  const columns = [
    {
      title: "STT",
      dataIndex: "index",
      key: "1",
    },
    {
      title: "Họ và tên",
      dataIndex: "fullName",
      key: "2",
    },
    {
      title: "Tên đăng nhập",
      dataIndex: "userName",
      key: "3",
    },
    {
      title: "Quyền hạn",
      dataIndex: "roleName",
      key: "4",
    },
    {
      title: "Tác vụ",
      dataIndex: "actions",
      key: "5",
    },
  ];
  const data = users.map((item, index) => {
    return {
      ...item,
      index: (pagination.pageIndex - 1) * pagination.pageSize + 1 + index,
      fullName: item.citizen.fullName,
      roleName: item.roleRef.name,
      actions: (
        <>
          <Popconfirm
            placement="topRight"
            title="Bạn có chắc chắn muốn xóa tài khoản này không?"
            onConfirm={() => onClickDelete(item.id)}
            okText="Yes"
            cancelText="No"
          >
            <Button>
              <DeleteOutlined />
            </Button>
          </Popconfirm>
        </>
      ),
    };
  });

  const onClickDelete = (id) => {
    instance
      .delete(`${API.user.delete}${id}`)
      .then(() => {
        toast.success("Xóa tài khoản thành công!");
        getAndSetData();
      })
      .catch((err) => {
        const message =
          err?.response?.data?.message || "Xóa tài khoản thất bại!";
        toast.error(message);
      });
  };
  const handleSubmitForm = (payload) => {
    if (!payload.id) {
      //   payload.createBy = payload.createBy || localStorage.getItem(USER_NAME);
      instance
        .post(API.user.create, payload)
        .then(() => {
          toast.success("Tạo tài khoản thành công!");
          setForm(defaultForm);
          getAndSetData();
        })
        .catch((err) => {
          const message = err?.response?.data?.message || "tạo khoản thất bại!";
          toast.error(message);
        });
    }
  };
  return (
    <div>
      <Card title="Quản lý tài khoản">
        <div style={{ textAlign: "right" }}>
          {" "}
          <Button
            type="primary"
            onClick={() =>
              setForm({
                ...defaultForm,
                isShowForm: !form.isShowForm,
              })
            }
          >
            Thêm mới
          </Button>
        </div>
        {isLoading ? (
          <Spin size="large" className="spin-loading" />
        ) : (
          <div className="table-user">
            <Table
              columns={columns}
              dataSource={data}
              bordered
              footer={() => (
                <div style={{ textAlign: "left" }}>Tổng {maxCounts}</div>
              )}
            />
          </div>
        )}

        <UserForm
          toggleForm={() => setForm(defaultForm)}
          visible={form.isShowForm}
          roleCategories={roleCategories}
          citizenHosts={citizenHosts}
          handleSubmit={handleSubmitForm}
          initialValues={{ ...form.modalUD }}
        />
      </Card>
    </div>
  );
}
