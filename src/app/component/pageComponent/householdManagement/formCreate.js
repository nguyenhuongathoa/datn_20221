import { MinusCircleOutlined, PlusOutlined } from "@ant-design/icons";
import { Button, Form, Input, Modal, Row, Col, Select, Space } from "antd";
import React, { useEffect, useState } from "react";
import { isactive } from "../../../config/const";
import SelectCustome from "../../../helpers/SelectCustome";
import "./style.css";
export default function HouseHoldCreateAndUpdateForm(props) {
  const handleOk = () => {
    const formValue = form.getFieldsValue(true);
    console.log(form);
    console.log(formValue);
    props.handleSubmit(formValue);
  };
  const handleCancel = () => {
    props.toggleForm();
  };
  const [form] = Form.useForm();
  const onFinish = (values) => {
    console.log(values);
  };
  const onFinishFailed = () => {};
  useEffect(() => {
    console.log("render");
    form.setFieldsValue({
      ...props.initialValues,
      status:
        props?.initialValues?.status === undefined ||
        props?.initialValues?.status
          ? 1
          : 0,
      listcitizen:
        props?.initialValues?.listHoldCitizens === undefined ||
        props?.initialValues?.listHoldCitizens
          ? props?.initialValues?.listHoldCitizens
          : null,
    });
  }, [props.initialValues]);
  return (
    <div className="household-form">
      <Modal
        title={
          props?.modalUD?.id ? "Cập nhật hộ gia đình" : "Thêm mới hộ gia đình"
        }
        visible={props.visible}
        onOk={handleOk}
        onCancel={handleCancel}
        width={1000}
      >
        <Form
          form={form}
          layout="vertical"
          name="basic"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <div className="row">
            <div className="col-6">
              {" "}
              <Form.Item label="Chủ hộ">
                <Form.Item
                  name="citizenId"
                  noStyle
                  rules={[
                    { required: true, message: "chủ hộ là trường bắt buộc" },
                  ]}
                >
                  <Select
                    placeholder="chọn công dân"
                    options={props.useHosts}
                  ></Select>
                </Form.Item>
              </Form.Item>
            </div>
            <div className="col-6">
              {" "}
              <Form.Item
                label="Số hộ khẩu"
                name="houseHoldNumber"
                initialValue=""
              >
                <Input />
              </Form.Item>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              {" "}
              <Form.Item
                label="Địa chỉ "
                name="address"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập địa chỉ!",
                  },
                ]}
                initialValue=""
              >
                <Input />
              </Form.Item>
            </div>
          </div>
          <div className="row">
            <div className="col-6">
              {" "}
              <Form.Item label="Thôn/Tổ">
                <Form.Item
                  name="groupAddress"
                  noStyle
                  rules={[{ required: true, message: "vui lòng chọn thôn/tổ" }]}
                >
                  <Select
                    placeholder="chọn thôn/tổ"
                    options={props.groupAddressCategories}
                  ></Select>
                </Form.Item>
              </Form.Item>
            </div>
            <div className="col-6">
              {" "}
              <Form.Item label="Trạng thái">
                <Form.Item
                  name="status"
                  noStyle
                  rules={[
                    { required: true, message: "vui lòng chọn trạng thái" },
                  ]}
                >
                  <Select
                    placeholder="chọn thôn/tổ"
                    options={props.statusHousehold}
                  ></Select>
                </Form.Item>
              </Form.Item>
            </div>
          </div>
          <Form.List name="listcitizen">
            {(fields, { add, remove }) => (
              <>
                {fields.map(({ key, name, ...restField }) => (
                  <Row
                    key={key}
                    style={{ display: "flex", marginBottom: 8 }}
                    align="baseline"
                  >
                    <Col xs={6}>
                      {" "}
                      <Form.Item
                        {...restField}
                        name={[name, "citizenId"]}
                        rules={[
                          {
                            required: true,
                            message: "Bắt buộc",
                          },
                        ]}
                      >
                        <Select
                          placeholder=" Chọn thành viên"
                          options={props.useHosts}
                        ></Select>
                      </Form.Item>
                    </Col>

                    <Col xs={6}>
                      <Form.Item
                        {...restField}
                        name={[name, "relationShipHouseHold"]}
                        rules={[{ required: true, message: "Bắt buộc" }]}
                      >
                        <Input placeholder="Quan hệ với chủ hộ" />
                      </Form.Item>
                    </Col>
                    <Col xs={6}>
                      {" "}
                      <Form.Item
                        {...restField}
                        name={[name, "statusType"]}
                        rules={[
                          {
                            required: true,
                            message: "Bắt buộc",
                          },
                        ]}
                      >
                        <Select
                          placeholder="Hình thức sinh sống"
                          options={props.statusCategories}
                        ></Select>
                      </Form.Item>
                    </Col>
                    <Col xs={5}>
                      <Form.Item
                        {...restField}
                        name={[name, "isactive"]}
                        rules={[
                          {
                            required: true,
                            message: "Bắt buộc",
                          },
                        ]}
                      >
                        <Select
                          placeholder="Trạng thái"
                          options={isactive}
                        ></Select>
                      </Form.Item>
                    </Col>

                    <MinusCircleOutlined onClick={() => remove(name)} />
                  </Row>
                ))}
                <Form.Item>
                  <Button
                    type="dashed"
                    onClick={() => add()}
                    block
                    icon={<PlusOutlined />}
                  >
                    thêm thành viên{" "}
                  </Button>
                </Form.Item>
              </>
            )}
          </Form.List>
        </Form>
      </Modal>
    </div>
  );
}
