import React, { useEffect, useState } from "react";
import API from "../../../config/api";
import instance from "../../../config/axiosClient";
import { toast } from "react-toastify";
import {
  GET_LIST_ERROR,
  ROUTER_CONST,
  statusHousehold,
  USER_NAME,
} from "../../../config/const";
import { Button, Card, Col, Input, Row, Table } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { formatDateDDMMYYYY, normalizeToOptions } from "../../../helpers/utlis";
import "./style.css";
import SelectCustome from "../../../helpers/SelectCustome";
import { Link } from "react-router-dom";
import HouseHoldCreateAndUpdateForm from "./formCreate";
import { Popconfirm, Spin } from "antd";

const pageSize = 100;
const defaultForm = {
  isShowForm: false,
  modalUD: {
    id: null,
    houseHoldNumber: "",
    groupAddress: "",
    citizenId: null,
    address: "",
    status: 1,
  },
};
const getFullNameAndId = (info) =>
  `${info?.fullName} - ${info?.indentificationNumber}`;
export default function HouseholdManagement() {
  const [form, setForm] = useState(defaultForm);
  const [households, sethouseholds] = useState([]);
  const [searchParams, setSearchParams] = useState({});
  const [maxCounts, setMaxCounts] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [groupAddressCategories, setGroupAddressCategories] = useState([]);
  const [useHosts, setUserHosts] = useState([]);
  const [statusCategories, setStatusCategories] = useState([]);
  const [dataTable, setDataTable] = useState([]);

  const [pagination, setPagination] = useState({
    pageSize: 100,
    pageIndex: 1,
    totalRecord: 0,
  });

  const getAndSetData = () => {
    const params = {
      ...searchParams,
      citizenNumber: searchParams?.citizenNumber,
      status: searchParams?.status,
    };
    setIsLoading(true);
    instance
      .get(API.household.getList, {
        params: { page: 1, size: pageSize, ...params },
      })
      .then((res) => {
        const data = res?.data?.data?.object;
        const max = res?.data?.data?.maxCount;

        console.log(data);
        const paging = {
          totalRecord: res?.data?.data?.maxCount,
          pageIndex: res?.data?.data?.pageIndex,
          pageSize: res?.data?.data?.pageSize,
        };
        sethouseholds(data);
        setPagination(paging);
        setIsLoading(false);
        setMaxCounts(max);

        const dataRender = data.map((item, index) => {
          return {
            ...item,
            index: (pagination.pageIndex - 1) * pagination.pageSize + 1 + index,
            createDate: formatDateDDMMYYYY(item.createDate),
            citizenNumber: item.citizenNumber ? item.citizenNumber : 1,
            groupAddressName: item.groupAddressRef
              ? item.groupAddressRef.name
              : null,
            actions: (
              <>
                <Button onClick={() => onClickEdit(item.id)}>
                  <EditOutlined />
                </Button>
                <Popconfirm
                  placement="topRight"
                  title="Bạn có chắc chắn muốn xóa hộ gia đình này không?"
                  onConfirm={() => onClickDelete(item.id)}
                  okText="Yes"
                  cancelText="No"
                >
                  <Button>
                    <DeleteOutlined />
                  </Button>
                </Popconfirm>
              </>
            ),
          };
        });
        setDataTable(dataRender);
      })
      .catch(() => {
        toast.error(GET_LIST_ERROR);
        setIsLoading(false);
      });
  };
  useEffect(() => {
    instance.get(`${API.reference.getCategory}GROUP_ADDRESS`).then((res) => {
      const options = res?.data?.data
        ? normalizeToOptions(res?.data?.data, "name", "code")
        : [];
      setGroupAddressCategories(options);
    });
    instance.get(`${API.reference.getCategory}STATUS_TYPE`).then((res) => {
      const options = res?.data?.data
        ? normalizeToOptions(res?.data?.data, "name", "code")
        : [];
      setStatusCategories(options);
    });

    getAndSetData();
  }, []);
  const columns = [
    {
      title: "STT",
      dataIndex: "index",
      key: "1",
    },
    {
      title: "Tên chủ hộ",
      dataIndex: "hostName",
      key: "2",
    },
    {
      title: "Tổ",
      dataIndex: "groupAddressName",
      key: "3",
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
      key: "4",
    },
    {
      title: "Số hộ khẩu",
      dataIndex: "houseHoldNumber",
      key: "5",
    },
    {
      title: "Số thành viên",
      dataIndex: "citizenNumber",
      key: "6",
    },
    {
      title: "Ngày tạo",
      dataIndex: "createDate",
      key: "7",
    },
    {
      title: "Tác vụ",
      dataIndex: "actions",
      key: "8",
    },
  ];

  const onClickEdit = (id) => {
    instance.get(`${API.citizen.getForHost}${id}`).then((res) => {
      const options = res?.data?.data
        ? normalizeToOptions(res?.data?.data, getFullNameAndId, "id")
        : [];
      setUserHosts(options);
    });
    instance.get(`${API.household.getID}${id}`).then((res) => {
      const options = res?.data?.data;
      console.log(options);
      setForm({
        modalUD: options,
        isShowForm: true,
      });
    });
  };

  const onClickDelete = (id) => {
    console.log("delete");
    instance
      .delete(`${API.household.delete}${id}`)
      .then(() => {
        toast.success("Xóa hộ gia đình thành công!");
        getAndSetData();
      })
      .catch((err) => {
        const messerr = err.response.data.message;
        toast.error(messerr ? messerr : "Xóa hộ gia đình thất bại!");
      });
  };
  const _setSearchParams = (value, field) => {
    const newState = {
      ...searchParams,
      [field]: value,
    };
    setSearchParams(newState);
  };
  const handleSubmitForm = (payload) => {
    if (!payload.id) {
      payload.createBy = payload.createBy || localStorage.getItem(USER_NAME);
      instance
        .post(API.household.create, payload)
        .then(() => {
          toast.success("Tạo hộ gia đình thành công!");
          setForm(defaultForm);
          getAndSetData();
        })
        .catch((err) => {
          const messerr = err.response.data.message;
          toast.error(messerr ? messerr : "Thêm hộ gia đình thất bại!");
        });
    } else {
      payload.updateBy = localStorage.getItem(USER_NAME);
      instance
        .put(API.household.update, payload)
        .then(() => {
          toast.success("Cập nhật hộ gia đình thành công!");
          setForm(defaultForm);
          getAndSetData();
        })
        .catch((err) => {
          console.log({ ...err });
          const messerr = err.response.data.message;
          toast.error(messerr ? messerr : "Cập nhật hộ gia đình thất bại!");
        });
    }
  };

  console.log(dataTable, "data");
  return (
    <Card title="Quản lý hộ dân">
      <div>
        <Row className="search-row mb-3">
          <Col span={4}>
            <Input
              placeholder="Tên thành viên/địa chỉ"
              onChange={(e) => _setSearchParams(e.target.value, "searchValue")}
            />
          </Col>
          <Col span={4}>
            <Input
              placeholder="Số thành viên"
              onChange={(e) =>
                _setSearchParams(e.target.value, "citizenNumber")
              }
            />
          </Col>
          <Col span={4}>
            <SelectCustome
              style={{ with: "100%" }}
              options={statusHousehold}
              placeholder="Chọn trạng thái"
              onChange={(e) => _setSearchParams(e, "status")}
            />
          </Col>
          <Button
            type="primary"
            onClick={getAndSetData}
            style={{ marginTop: 4 }}
          >
            Tìm kiếm
          </Button>
        </Row>
      </div>
      <div style={{ textAlign: "right" }}>
        {" "}
        <Button
          type="primary"
          onClick={() => {
            instance.get(`${API.citizen.getForHost}0`).then((res) => {
              const options = res?.data?.data
                ? normalizeToOptions(res?.data?.data, getFullNameAndId, "id")
                : [];
              setUserHosts(options);
            });
            setForm({
              ...defaultForm,
              isShowForm: !form.isShowForm,
            });
          }}
        >
          Thêm mới
        </Button>
      </div>

      {isLoading ? (
        <Spin size="large" className="spin-loading" />
      ) : (
        <div className="table-household">
          <Table
            columns={columns}
            dataSource={dataTable?.length > 0 ? dataTable : []}
            bordered
            footer={() => (
              <div style={{ textAlign: "left" }}>Tổng {maxCounts}</div>
            )}
          />
        </div>
      )}
      <HouseHoldCreateAndUpdateForm
        toggleForm={() => setForm(defaultForm)}
        visible={form.isShowForm}
        groupAddressCategories={groupAddressCategories}
        useHosts={useHosts}
        statusHousehold={statusHousehold}
        statusCategories={statusCategories}
        handleSubmit={handleSubmitForm}
        initialValues={{ ...form.modalUD }}
      />
    </Card>
  );
}
