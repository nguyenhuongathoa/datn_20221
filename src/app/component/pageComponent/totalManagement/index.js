import React, { useEffect, useState } from "react";
import "./style.css";
import ReactApexChart from "react-apexcharts";
import API from "../../../config/api";
import instance from "../../../config/axiosClient";
import { Button } from "antd";
const seriesVacin = [120, 170, 150];
const lableVaxin = ["1 mũi", "2 mũi", "3 mũi"];
const statechar = (serie = [], label = []) => {
  return {
    series: serie,
    options: {
      chart: {
        width: 280,
        type: "pie",
      },
      labels: label,
      responsive: [
        {
          breakpoint: 700,
          options: {
            chart: {
              width: 400,
            },
            legend: {
              position: "bottom",
            },
          },
        },
      ],
    },
  };
};

// const processResponse = (res) => {
//   const chardto = {
//     serie: res?.data?.data?.series,
//     label: res?.data?.data?.labels,
//   };
//   return statechar(chardto?.serie, chardto?.label);
// };

// const defaulData = {
//   options: {
//     chart: {
//       width: 380,
//       type: "pie",
//     },
//     responsive: [
//       {
//         breakpoint: 600,
//         options: {
//           chart: {
//             width: 200,
//           },
//           legend: {
//             position: "bottom",
//           },
//         },
//       },
//     ],
//   },
//   series: [],
// };

export default function TotalManagement() {
  const [sexCitizen, setSexCitizen] = useState();
  const [vaccinCitizen, setVaccinCitizen] = useState();
  const [ageCitizen, setAgeCitizen] = useState();
  const [nationCitizen, setNationCitizen] = useState();
  const [educationCitizen, setEducationCitizen] = useState();
  const [religionCitizen, setReligionCitizen] = useState();
  useEffect(() => {
    instance.get(API.total.sex).then((res) => {
      setSexCitizen(res.data.data);
    });
    instance.get(API.total.vaccin).then((res) => {
      setVaccinCitizen(res.data.data);
    });
    instance.get(API.total.age).then((res) => {
      setAgeCitizen(res.data.data);
    });
    instance.get(API.total.nation).then((res) => {
      setNationCitizen(res.data.data);
    });
    instance.get(API.total.education).then((res) => {
      setEducationCitizen(res.data.data);
    });

    instance.get(API.total.religion).then((res) => {
      console.log(res.data.data, "res religion");
      setReligionCitizen(res.data.data);
    });
  }, []);

  return (
    <div>
      <div className="row">
        <div className="col-10">
          <h6>Tổng quan dân số hiện tại</h6>
        </div>
        {/* <div className="col-2">
          <Button
            type="primary"
            // onClick={() =>
            //   setForm({
            //     ...defaultForm,
            //     isShowForm: !form.isShowForm,
            //   })
            // }
          >
            Lưu Thống kê
          </Button>
        </div> */}
      </div>

      <div className="row">
        <div className="col-4">
          <div className="chart">
            {sexCitizen?.series && (
              <ReactApexChart
                options={
                  statechar(sexCitizen?.series, sexCitizen?.labels)?.options
                }
                series={
                  statechar(sexCitizen?.series, sexCitizen?.labels)?.series
                }
                type="pie"
                width={300}
              />
            )}
          </div>
          <div className="title-Char">
            <h5>Tỉ lệ giới tính</h5>
          </div>
        </div>
        <div className="col-4">
          <div className="chart">
            {ageCitizen?.series && (
              <ReactApexChart
                options={
                  statechar(ageCitizen?.series, ageCitizen?.labels)?.options
                }
                series={
                  statechar(ageCitizen?.series, ageCitizen?.labels)?.series
                }
                type="pie"
                width={349}
              />
            )}
          </div>
          <div className="title-Char">
            <h5>Tỉ lệ độ tuổi</h5>
          </div>
        </div>
        <div className="col-4">
          <div className="chart">
            {vaccinCitizen?.series && (
              <ReactApexChart
                options={
                  statechar(vaccinCitizen?.series, vaccinCitizen?.labels)
                    ?.options
                }
                series={
                  statechar(vaccinCitizen?.series, vaccinCitizen?.labels)
                    ?.series
                }
                type="pie"
                width={300}
              />
            )}
          </div>
          <div className="title-Char">
            <h5>Tỉ lệ tiêm phòng</h5>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-4">
          <div className="chart">
            {educationCitizen?.series && (
              <ReactApexChart
                options={
                  statechar(educationCitizen?.series, educationCitizen?.labels)
                    ?.options
                }
                series={
                  statechar(educationCitizen?.series, educationCitizen?.labels)
                    ?.series
                }
                type="pie"
                width={390}
              />
            )}
          </div>
          <div className="title-Char">
            <h5>Tỉ lệ trình độ giáo dục</h5>
          </div>
        </div>
        <div className="col-4">
          <div className="chart">
            {nationCitizen?.series && (
              <ReactApexChart
                options={
                  statechar(nationCitizen?.series, nationCitizen?.labels)
                    ?.options
                }
                series={
                  statechar(nationCitizen?.series, nationCitizen?.labels)
                    ?.series
                }
                type="pie"
                width={348}
              />
            )}
          </div>
          <div className="title-Char">
            <h5>Tỉ lệ dân tộc</h5>
          </div>
        </div>
        <div className="col-4">
          <div className="chart">
            {religionCitizen?.series && (
              <ReactApexChart
                options={
                  statechar(religionCitizen?.series, religionCitizen?.labels)
                    ?.options
                }
                series={
                  statechar(religionCitizen?.series, religionCitizen?.labels)
                    ?.series
                }
                type="pie"
                width={300}
              />
            )}
          </div>
          <div className="title-Char">
            <h5>Tỉ lệ Tôn giáo</h5>
          </div>
        </div>
      </div>
    </div>
  );
}
