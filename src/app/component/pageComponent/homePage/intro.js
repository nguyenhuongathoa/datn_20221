import React from "react";

export default function TotalForm() {
  return (
    <div>
      <h4>Phường Mai Động</h4>
      <p>
        Mai Động là một phường thuộc quận Hoàng Mai, thành phố Hà Nội, Việt Nam,
        có trụ sở UBND tại Số 25A, ngõ 13, đường Lĩnh Nam.
      </p>

      <p>
        Phường Mai Động có diện tích tự nhiên là 0,82 km², dân số năm 2012 là
        20.000 người, mật độ dân số 24.390 người/km², dân tộc chủ yếu là dân tộc
        kinh.
      </p>
      <h4>Địa lý</h4>
      <p>Địa giới hành chính của phường này như sau:</p>
      <ul>
        <li>Nam giáp phường Hoàng Văn Thụ</li>
        <li>Bắc giáp phường Minh Khai, quận Hai Bà Trưng</li>
        <li>Đông giáp phường Vĩnh Hưng</li>
        <li>Tây giáp phường Hoàng Văn Thụ.</li>
      </ul>

      <h4>Lịch sử</h4>
      <p>
        Mai Động có tên cổ là My Động. Năm 1979, khi nạo vét sông Kim Ngưu,
        những công cụ bằng đá có niên đại khoảng từ 3.500 năm đến 4.000 năm đã
        được phát hiện.
      </p>
      <p>
        Vào những năm đầu Công nguyên, Tam Trinh, người quận Cửu Chân, đến Mai
        Động mở trường dạy văn và võ. Năm 40, khi Hai Bà Trưng phất cờ khởi
        nghĩa, Tam Trinh đem 3.000 tráng đinh lên Hát Môn tụ nghĩa.
      </p>

      <p>
        Đầu thế kỷ XV, Mai Động là nơi diễn ra một trận chiến giữa quân Minh do
        Vương Thông chỉ huy và nghĩa quân của Lê Lợi do Thái giám Lê Nguyễn chỉ
        huy.
      </p>
      <h4>Hành Chính</h4>
      <p>
        Phường Mai Động được thành lập ngày 13 tháng 10 năm 1982 trên cơ sở tách
        thôn Mai Động và xóm Mơ Táo của xã Hoàng Văn Thụ thuộc huyện Thanh Trì
        cũ, lúc đầu phường thuộc quận Hai Bà Trưng.
      </p>
      <p>
        Từ 1 tháng 1 năm 2004, phường Mai Động chuyển sang trực thuộc quận Hoàng
        Mai.
      </p>
      <h4>Tham khảo</h4>
      <a href="https://vi.wikipedia.org/wiki/Mai_%C4%90%E1%BB%99ng_(ph%C6%B0%E1%BB%9Dng)">
        wikipedia
      </a>
    </div>
  );
}
