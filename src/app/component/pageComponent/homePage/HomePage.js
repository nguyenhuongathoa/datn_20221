import { Button, Carousel, Layout, Menu } from "antd";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import API from "../../../config/api";
import instance from "../../../config/axiosClient";
import { ROUTER_CONST } from "../../../config/const";
import "./homePage.css";
import moment from "moment";
import image1 from "../../../assets/Mai động/1.png";
import image10 from "../../../assets/Mai động/10.jpg";
import image5 from "../../../assets/Mai động/5.jpg";
import image9 from "../../../assets/Mai động/9.jpg";
import imageicon from "../../../assets/Mai động/icon.png";
import TotalForm from "./intro";

const { Content } = Layout;
const typeNews = {
  new: "NEW",
  activity: "ACTIVITY",
  policy: "POLICY",
  total: "TOTAL",
};
const pageSize = 5;

const contentStyle = {
  height: "160px",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  background: "#364d79",
};
function HomePageComponent() {
  const [current, setcurrent] = useState(typeNews.new);
  const [subList, setSubList] = useState([]);
  const handleClick = (e) => {
    setcurrent(e.key);
  };

  useEffect(() => {
    if (current !== "TOTAL") {
      instance
        .get(API.news.getList, {
          params: { type: current, page: 1, size: pageSize, status: "APPROVE" },
        })
        .then((res) => {
          const data = res?.data?.data?.object;
          setSubList(data);
        });
    } else {
      // const data = (template = { __html: { __html } });
      setSubList([]);
    }
  }, [current]);
  return (
    <Content className="image-slide content-home-page">
      {/* <Space align="center"> */}
      <div className="header">
        <div className="header-left">
          <img width={140} src={imageicon}></img>
          <div>
            <h1>QUẬN HOÀNG MAI</h1>
            <h2>THÔNG TIN PHƯỜNG MAI ĐỘNG</h2>
          </div>
        </div>
        <Link to="/login">
          <Button className="btn-login">Đăng nhập</Button>
        </Link>
        {/* </Space> */}
      </div>
      <Carousel
        autoplay
        style={{
          width: "100%",
          height: "400px",
        }}
      >
        <div style={{ background: "#364d79", width: "100%" }}>
          <img
            style={{ height: "400px", margin: "auto" }}
            src={image1}
            className="item-image"
          ></img>
        </div>
        <div>
          <img
            style={{ height: "400px", margin: "auto" }}
            src={image10}
            className="item-image"
          ></img>
        </div>
        <div>
          <img
            style={{ height: "400px", margin: "auto" }}
            src={image5}
            className="item-image"
          ></img>
        </div>
        <div>
          <img
            style={{ height: "400px", margin: "auto" }}
            src={image9}
            className="item-image"
          ></img>
        </div>
      </Carousel>
      ,
      <Menu onClick={handleClick} selectedKeys={[current]} mode="horizontal">
        <Menu.Item key={typeNews.total}>TỔNG QUÁT</Menu.Item>
        <Menu.Item key={typeNews.new}>TIN TỨC</Menu.Item>
        <Menu.Item key={typeNews.activity}>HOẠT ĐỘNG</Menu.Item>
        <Menu.Item key={typeNews.policy}>CHÍNH SÁCH</Menu.Item>
      </Menu>
      <div className="site-layout-background">
        {current === typeNews.total ? (
          <>
            <TotalForm />
          </>
        ) : (
          <>
            {subList.map((newa) => {
              return (
                <div class="item">
                  <div class="item-content item-left calender-date">
                    <div class="month">
                      Tháng {moment(newa.createDate).month()}
                    </div>
                    <div class="date"> {moment(newa.createDate).date()}</div>
                  </div>
                  <div class="item-content item-right">
                    <Link to={`${ROUTER_CONST.news}/${newa.id}`}>
                      <p class="title">
                        <b>{newa.noticeCategory}</b> {newa.subject}
                      </p>
                      <p class="datetime">
                        {moment(newa.createDate).format("DD/MM/YYYY")}
                      </p>
                    </Link>
                  </div>
                </div>
              );
            })}
          </>
        )}
      </div>
      <div style={{ textAlign: "center", fontWeight: 600, marginTop: 10 }}>
        <div>Phường Mai Động</div>
        <div style={{ marginBottom: 0 }}>
          Đ/c: 13 Đ. Lĩnh Nam, Mai Động, Hoàng Mai, Hà Nội
        </div>
      </div>
    </Content>
  );
}

export default HomePageComponent;
