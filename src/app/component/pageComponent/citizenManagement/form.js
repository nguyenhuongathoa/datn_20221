import { DatePicker, Form, Input, InputNumber, Modal } from "antd";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { statusAlive, statusNotice } from "../../../config/const";
import RichTextEditor from "../../../helpers/RichTextEditor";
import SelectCustome from "../../../helpers/SelectCustome";
import { normalizeToOptions } from "../../../helpers/utlis";
import "./style.css";

export default function CitizenForm(props) {
  const handleOk = () => {
    const formValue = form.getFieldsValue(true);
    console.log(form);
    console.log(formValue);
    props.handleSubmit(formValue);
  };
  const handleCancel = () => {
    props.toggleForm();
  };
  const [form] = Form.useForm();
  const onFinish = (values) => {
    console.log(values);
  };
  const onFinishFailed = () => {};
  useEffect(() => {
    console.log("render");
    form.setFieldsValue({
      ...props.initialValues,
      statusAlive:
        props?.initialValues?.statusAlive === undefined ||
        props?.initialValues?.statusAlive
          ? 1
          : 0,
      dateOfBirth: props?.initialValues?.dateOfBirth
        ? moment(props.initialValues.dateOfBirth)
        : null,
    });
  }, [props.initialValues]);
  return (
    <div className="notice-form">
      <Modal
        title={props?.modalUD?.id ? "Cập nhật công dân" : "Thêm mới công dân"}
        visible={props.visible}
        onOk={handleOk}
        onCancel={handleCancel}
        className="form-updata-create"
        width={1000}
      >
        <Form
          form={form}
          layout="vertical"
          name="basic"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          // initialValues={{
          //   ...props.initialValues,
          //   statusAlive: props?.initialValues?.statusAlive ? 1 : 0,
          // }}
        >
          <div className="row">
            <div className="col-4">
              <Form.Item
                label="Họ và Tên"
                name="fullName"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập họ và tên!",
                  },
                ]}
                initialValue=""
              >
                <Input />
              </Form.Item>
            </div>
            <div className="col-4">
              <Form.Item
                label="Giới tính"
                name="sex"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng chọn giới tính!",
                  },
                ]}
              >
                <SelectCustome
                  placeholder="Chọn giới tính"
                  options={props.sexs}
                />
              </Form.Item>
            </div>
            <div className="col-4">
              <Form.Item
                label="Ngày Sinh"
                name="dateOfBirth"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập ngày tháng năm sinh!",
                  },
                ]}
                initialValue=""
              >
                <DatePicker />
              </Form.Item>
            </div>
          </div>
          <div className="row">
            <div className="col-4">
              <Form.Item
                label="Dân Tộc"
                name="nation"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập dân tộc",
                  },
                ]}
                initialValue=""
              >
                <Input />
              </Form.Item>
            </div>
            <div className="col-4">
              <Form.Item
                label="Tôn giáo"
                name="religion"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập tôn giáo",
                  },
                ]}
                initialValue=""
              >
                <Input />
              </Form.Item>
            </div>
            <div className="col-4">
              <Form.Item
                label="Quốc tịch"
                name="countyName"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập quốc tịch",
                  },
                ]}
                initialValue=""
              >
                <Input />
              </Form.Item>
            </div>
          </div>
          <div className="row">
            <div className="col-4">
              <Form.Item
                label="Loại giấy tờ định danh"
                name="indentificationType"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng chọn loại giấy tờ định danh!",
                  },
                ]}
              >
                <SelectCustome
                  placeholder="Chọn loại giấy tờ định danh"
                  options={props.indentificationType}
                />
              </Form.Item>
            </div>
            <div className="col-4">
              <Form.Item
                label="Số CMND/CCCD"
                name="indentificationNumber"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập số CMND/CCCD!",
                  },
                ]}
                initialValue=""
              >
                <Input />
              </Form.Item>
            </div>
            <div className="col-4">
              <Form.Item label="Số Hộ chiếu" name="passport" initialValue="">
                <Input />
              </Form.Item>
            </div>
          </div>
          <div className="row">
            <div className="col-4">
              <Form.Item
                label="Số điện thoại"
                name="phoneNumber"
                initialValue=""
              >
                <Input />
              </Form.Item>
            </div>
            <div className="col-4">
              <Form.Item label="Thư điện tử" name="email" initialValue="">
                <Input />
              </Form.Item>
            </div>
            <div className="col-4">
              <Form.Item
                label="Trạng thái"
                name="statusAlive"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng chọn trạng thái!",
                  },
                ]}
              >
                <SelectCustome
                  options={statusAlive}
                  placeholder="Chọn trạng thái"
                />
              </Form.Item>
            </div>
          </div>
          <div className="row">
            <Form.Item label="Nguyên quán" name="regionBirth" initialValue="">
              <Input />
            </Form.Item>
          </div>
          <div className="row">
            <Form.Item
              label="Địa chỉ thường trú"
              name="residentAddress"
              initialValue=""
            >
              <Input />
            </Form.Item>
          </div>
          <div className="row">
            <Form.Item
              label="Địa chỉ tạm trú"
              name="temporaryResidenceAddress"
              initialValue=""
            >
              <Input />
            </Form.Item>
          </div>
          <div className="row">
            <div className="col-6">
              <Form.Item label="Đang theo học" name="education">
                <SelectCustome
                  placeholder="Chọn đang theo học"
                  options={props.educations}
                />
              </Form.Item>
            </div>
            <div className="col-6">
              <Form.Item
                label="Trình độ giáo dục cao nhất"
                name="educationGraduate"
              >
                <SelectCustome
                  placeholder="Chọn trình độ giao dục cao nhất"
                  options={props.educationGraduateS}
                />
              </Form.Item>
            </div>
          </div>
          <div className="row">
            <div className="col-6">
              <Form.Item
                label="Chuyên ngành"
                name="speciallized"
                initialValue=""
              >
                <Input />
              </Form.Item>
            </div>
            <div className="col-6">
              <Form.Item label="Nghề nghiệp" name="job" initialValue="">
                <Input />
              </Form.Item>
            </div>
          </div>
          <div className="row">
            <div className="col-6">
              <Form.Item label="Mô tả" name="description" initialValue="">
                <Input />
              </Form.Item>
            </div>
            <div className="col-6">
              <Form.Item
                label="Số mũi vacxin đã tiêm"
                name="vaccination"
                initialValue=""
              >
                <InputNumber />
              </Form.Item>
            </div>
          </div>
        </Form>
      </Modal>
    </div>
  );
}
