import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Button, Card, Table, Col, Input, Row, Popconfirm, Spin } from "antd";
import React, { useState, useEffect } from "react";
import { toast } from "react-toastify";
import API from "../../../config/api";
import instance from "../../../config/axiosClient";
import { GET_LIST_ERROR, statusAlive, USER_NAME } from "../../../config/const";
import { formatDateDDMMYYYY, normalizeToOptions } from "../../../helpers/utlis";
import "./style.css";
import CitizenForm from "./form";
import SelectCustome from "../../../helpers/SelectCustome";

//import { Link } from "react-router-dom";

const pageSize = 100;

export default function CitizenManagement() {
  const [citizens, setCitizens] = useState([]);
  const [sexs, setSex] = useState([]);
  const [indentificationCategorys, setindentificationCategorys] = useState([]);
  const [educations, setEducations] = useState([]);
  const [educationGraduateS, seteducationGraduateS] = useState([]);
  const [searchParams, setSearchParams] = useState({});
  const [maxCounts, setMaxCounts] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const [pagination, setPagination] = useState({
    pageSize: 100,
    pageIndex: 1,
    totalRecord: 0,
  });
  const getAndSetData = () => {
    const params = {
      ...searchParams,
      education: searchParams?.education,
      sex: searchParams?.sex,
      statusAlive: searchParams?.statusAlive,
      indentificationNumber: searchParams?.indentificationNumber,
      religion: searchParams?.religion,
      nation: searchParams?.nation,
      educationGraduate: searchParams?.educationGraduate,
      speciallized: searchParams?.speciallized,
      vaccination: searchParams?.vaccination,
      ageTo: searchParams?.ageTo,
      ageFrom: searchParams?.ageFrom,
    };
    setIsLoading(true);
    instance
      .get(API.citizen.getList, {
        params: { page: 1, size: pageSize, ...params },
      })
      .then((res) => {
        const data = res?.data?.data?.object;
        const max = res?.data?.data?.maxCount;
        const paging = {
          totalRecord: res?.data?.data?.maxCount,
          pageIndex: res?.data?.data?.pageIndex,
          pageSize: res?.data?.data?.pageSize,
        };
        setCitizens(data);
        setPagination(paging);
        setIsLoading(false);
        setMaxCounts(max);
      })
      .catch(() => {
        toast.error(GET_LIST_ERROR);
      });
  };

  useEffect(() => {
    instance.get(`${API.reference.getCategory}SEX_TYPE`).then((res) => {
      const options = res?.data?.data
        ? normalizeToOptions(res?.data?.data, "name", "code")
        : [];
      setSex(options);
    });
    instance.get(`${API.reference.getCategory}ID_CATEGORY`).then((res) => {
      const options = res?.data?.data
        ? normalizeToOptions(res?.data?.data, "name", "code")
        : [];
      setindentificationCategorys(options);
    });
    instance.get(`${API.reference.getCategory}EDUCATION_TYPE`).then((res) => {
      const options = res?.data?.data
        ? normalizeToOptions(res?.data?.data, "name", "code")
        : [];
      setEducations(options);
    });
    instance
      .get(`${API.reference.getCategory}EDUCATION_GRADUATE_TYPE`)
      .then((res) => {
        const options = res?.data?.data
          ? normalizeToOptions(res?.data?.data, "name", "code")
          : [];
        seteducationGraduateS(options);
      });
    getAndSetData();
  }, []);
  const columns = [
    {
      title: "STT",
      dataIndex: "index",
      key: "1",
    },
    {
      title: "Họ và Tên",
      dataIndex: "fullName",
      key: "2",
    },
    {
      title: "Ngày Sinh",
      dataIndex: "dateOfBirth",
      key: "3",
    },
    {
      title: "Giới Tính",
      dataIndex: "sex",
      key: "4",
    },
    {
      title: "Dân Tộc",
      dataIndex: "nation",
      key: "5",
    },
    {
      title: "Tôn Giáo",
      dataIndex: "religion",
      key: "6",
    },
    {
      title: "CMND/CCCD",
      dataIndex: "indentificationNumber",
      key: "7",
    },
    {
      title: "Nơi Ở Hiện Tại",
      dataIndex: "residentAddress", //dùng tạm trường tạm trú
      key: "8",
    },
    {
      title: "Tác Vụ",
      dataIndex: "actions",
      key: "9",
    },
  ];
  const data = citizens.map((item, index) => {
    return {
      ...item,
      index: (pagination.pageIndex - 1) * pagination.pageSize + 1 + index,
      dateOfBirth: formatDateDDMMYYYY(item.dateOfBirth),
      sex: item.sexRef.name,
      actions: (
        <>
          <Button onClick={() => onClickEdit(item)}>
            <EditOutlined />
          </Button>
          <Popconfirm
            placement="topRight"
            title="Bạn có chắc chắn muốn xóa công dân này không?"
            onConfirm={() => onClickDelete(item.id)}
            okText="Yes"
            cancelText="No"
          >
            <Button>
              <DeleteOutlined />
            </Button>
          </Popconfirm>
        </>
      ),
    };
  });
  const onClickEdit = (data) => {
    console.log(data);
    setForm({
      modalUD: data,
      isShowForm: true,
    });
  };
  const onClickDelete = (id) => {
    instance
      .delete(`${API.citizen.delete}${id}`)
      .then(() => {
        toast.success("Xóa thông báo thành công!");
        getAndSetData();
      })
      .catch((err) => {
        const message =
          err?.response?.data?.message || "Xóa thông báo thất bại!";
        toast.error(message);
      });
  };

  const defaultForm = {
    isShowForm: false,
    modalUD: {
      id: null,
      fullName: null,
      dateOfBirth: null,
      sex: null,
      indentificationType: null,
      indentificationNumber: null,
      passport: null,
      phoneNumber: null,
      email: null,
      nation: null,
      religion: null,
      countyName: null,
      regionBirth: null,
      residentAddress: null,
      temporaryResidenceAddress: null,
      education: null,
      educationGraduate: null,
      speciallized: null,
      job: null,
      statusAlive: true,
      description: null,
      createBy: null,
      vaccination: null,
    },
  };
  const [form, setForm] = useState(defaultForm);
  const handleSubmitForm = (payload) => {
    console.log(payload);
    if (!payload.id) {
      payload.createBy = payload.createBy || localStorage.getItem(USER_NAME);
      instance
        .post(API.citizen.create, payload)
        .then(() => {
          toast.success("Tạo Công dân thành công!");
          setForm(defaultForm);
          getAndSetData();
        })
        .catch((err) => {
          console.log({ ...err });
          toast.error("Thêm Công dân thất bại!");
        });
    } else {
      instance
        .put(API.citizen.update, payload)
        .then(() => {
          toast.success("Cập nhật thông báo thành công!");
          setForm(defaultForm);
          getAndSetData();
        })
        .catch((err) => {
          console.log({ ...err });
          toast.error("Cập nhật thông báo thất bại!");
        });
    }
  };

  const _setSearchParams = (value, field) => {
    const newState = {
      ...searchParams,
      [field]: value,
    };
    setSearchParams(newState);
  };
  return (
    <Card title="Quản lý công dân">
      <Row className="search-row mb-3">
        <Col span={4}>
          <Input
            placeholder="Tên/Chủ hộ/Quốc tịch/Địa chỉ"
            onChange={(e) => _setSearchParams(e.target.value, "searchValue")}
          />
        </Col>
        <Col span={4}>
          <Input
            placeholder="CMND/CCCD/Sđt/Email/Passport"
            onChange={(e) =>
              _setSearchParams(e.target.value, "indentificationNumber")
            }
          />
        </Col>
        <Col span={4}>
          <Input
            placeholder="Tôn giáo"
            onChange={(e) => _setSearchParams(e.target.value, "religion")}
          />
        </Col>
        <Col span={4}>
          <Input
            placeholder="Dân tộc"
            onChange={(e) => _setSearchParams(e.target.value, "nation")}
          />
        </Col>
        <Col span={4}>
          <SelectCustome
            options={sexs}
            placeholder="Chọn giới tính"
            onChange={(e) => _setSearchParams(e, "sex")}
          />
        </Col>
        <Col span={4}>
          <SelectCustome
            options={educations}
            value={searchParams?.education}
            placeholder="Chọn đang theo học"
            onChange={(e) => _setSearchParams(e, "education")}
          />
        </Col>
        <Col span={4}>
          <SelectCustome
            options={educationGraduateS}
            value={searchParams?.educationGraduate}
            placeholder="Trình độ đào tạo cao nhất"
            onChange={(e) => _setSearchParams(e, "educationGraduate")}
          />
        </Col>
        <Col span={4}>
          <Input
            placeholder="Chuyên Ngành/Nghề nghiệp"
            onChange={(e) => _setSearchParams(e.target.value, "speciallized")}
          />
        </Col>
        <Col span={4}>
          <Input
            placeholder="Số mũi đã tiêm"
            onChange={(e) => _setSearchParams(e.target.value, "vaccination")}
          />
        </Col>
        <Col span={4}>
          <Input
            placeholder="Tuổi từ"
            onChange={(e) => _setSearchParams(e.target.value, "ageTo")}
          />
        </Col>
        <Col span={4}>
          <Input
            placeholder="Đến tuổi"
            onChange={(e) => _setSearchParams(e.target.value, "ageFrom")}
          />
        </Col>
        <Col span={4}>
          <SelectCustome
            options={statusAlive}
            placeholder="Chọn trạng thái"
            onChange={(e) => _setSearchParams(e, "statusAlive")}
          />
        </Col>
      </Row>
      <div className="actions-row mb-3">
        <Button type="primary" onClick={getAndSetData}>
          Tìm kiếm
        </Button>
        <Button
          type="primary"
          onClick={() =>
            setForm({
              ...defaultForm,
              isShowForm: !form.isShowForm,
            })
          }
        >
          Thêm mới
        </Button>
      </div>

      {isLoading ? (
        <Spin size="large" className="spin-loading" />
      ) : (
        <div className="table-notice">
          <Table
            columns={columns}
            dataSource={data}
            bordered
            footer={() => (
              <div style={{ textAlign: "left" }}>Tổng {maxCounts}</div>
            )}
          />
        </div>
      )}
      <CitizenForm
        toggleForm={() => setForm(defaultForm)}
        visible={form.isShowForm}
        sexs={sexs} //noticeCategories
        indentificationType={indentificationCategorys}
        educations={educations}
        educationGraduateS={educationGraduateS}
        handleSubmit={handleSubmitForm}
        initialValues={{ ...form.modalUD }}
      />
    </Card>
  );
}
