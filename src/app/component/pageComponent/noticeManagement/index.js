import {
  Button,
  Card,
  Col,
  DatePicker,
  Input,
  Row,
  Table,
  Spin,
  Popconfirm,
} from "antd";
import React, { useEffect, useState } from "react";
import API from "../../../config/api";
import instance from "../../../config/axiosClient";
import {
  formatDateDDMMYYYY,
  formatDateYYYYMMDD,
  normalizeToOptions,
} from "../../../helpers/utlis";
import NoticeForm from "./form";
import { toast } from "react-toastify";
import { GET_LIST_ERROR, statusNotice, USER_NAME } from "../../../config/const";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
import SelectCustome from "../../../helpers/SelectCustome";
import { useLocation } from "react-router-dom";
import { useParams } from "react-router";
import NoticeFormAppove from "./formApprove";
const pageSize = 100;
const defaultForm = {
  isShowForm: false,
  modalUD: {
    id: null,
    subject: "",
    noticeCategory: null,
    status: 1,
    noticecontent: "",
  },
};
export default function NoticeManagement(props) {
  const [notices, setNotices] = useState([]);
  const [pagination, setPagination] = useState({
    pageSize: 100,
    pageIndex: 1,
    totalRecord: 0,
  });
  const [form, setForm] = useState(defaultForm);
  const [noticeCategories, setNoticeCategories] = useState([]);
  const [maxCounts, setMaxCounts] = useState([]);
  const [statusCategories, setStatusCategories] = useState([]);
  const [searchParams, setSearchParams] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const location = useLocation();
  const { status } = useParams();

  const getAndSetData = () => {
    const params = {
      ...searchParams,
      type: searchParams?.type,
      // status: searchParams?.status,
    };
    setIsLoading(true);
    instance
      .get(API.news.getList, {
        params: { page: 1, size: pageSize, status: status, ...params },
      })
      .then((res) => {
        const data = res?.data?.data?.object;
        const max = res?.data?.data?.maxCount;
        const paging = {
          totalRecord: res?.data?.data?.maxCount,
          pageIndex: res?.data?.data?.pageIndex,
          pageSize: res?.data?.data?.pageSize,
        };
        setNotices(data);
        setPagination(paging);
        setIsLoading(false);
        setMaxCounts(max);
      })
      .catch(() => {
        toast.error(GET_LIST_ERROR);
        setIsLoading(false);
      });
  };
  useEffect(() => {
    instance.get(`${API.reference.getCategory}NOTICE_CATEGORY`).then((res) => {
      const options = res?.data?.data
        ? normalizeToOptions(res?.data?.data, "name", "code")
        : [];
      setNoticeCategories(options);
    });
    instance.get(`${API.reference.getCategory}STATUS_NOTICE`).then((res) => {
      const options = res?.data?.data
        ? normalizeToOptions(res?.data?.data, "name", "code")
        : [];
      setStatusCategories(options);
    });
    getAndSetData();
  }, [status]);
  const columns = [
    {
      title: "STT",
      dataIndex: "index",
      key: "1",
    },
    {
      title: "Ngày tạo",
      dataIndex: "createdDate",
      key: "2",
    },
    {
      title: "Tiêu đề",
      dataIndex: "subject",
      key: "3",
    },
    {
      title: "Phân loại",
      dataIndex: "noticeCategory",
      key: "4",
    },
    {
      title: "Trạng thái",
      dataIndex: "status",
      key: "5",
    },
    {
      title: "Người cập nhật",
      dataIndex: "updatedBy",
      key: "6",
    },
    {
      title: "Ngày cập nhật",
      dataIndex: "updatedDate",
      key: "7",
    },
    {
      title: "Tác vụ",
      dataIndex: "actions",
      key: "8",
    },
  ];

  const data = notices.map((item, index) => {
    return {
      ...item,
      index: (pagination.pageIndex - 1) * pagination.pageSize + 1 + index,
      createdDate: formatDateDDMMYYYY(item.createDate),
      noticeCategory: item.noticeCategoryRef.name,
      status: item.statusRef.name,
      updatedBy: item.updateBy ? item.updateBy : item.createBy,
      updatedDate: item.updateDate
        ? formatDateDDMMYYYY(item.updateDate)
        : formatDateDDMMYYYY(item.createDate),
      actions: (
        <div key={index}>
          <Button onClick={() => onClickEdit(item)}>
            <EditOutlined />
          </Button>
          <Popconfirm
            placement="topRight"
            title="Bạn có chắc chắn muốn xóa thông báo này không?"
            onConfirm={() => onClickDelete(item.id)}
            okText="Yes"
            cancelText="No"
          >
            <Button>
              <DeleteOutlined />
            </Button>
          </Popconfirm>
        </div>
      ),
    };
  });

  const onClickEdit = (data) => {
    setForm({
      modalUD: data,
      isShowForm: true,
    });
  };

  const onClickDelete = (id) => {
    console.log("delete");
    instance
      .delete(`/v1/notice/delete/${id}`)
      .then(() => {
        toast.success("Xóa thông báo thành công!");
        getAndSetData();
      })
      .catch((err) => {
        const message =
          err?.response?.data?.message || "Xóa thông báo thất bại!";
        toast.error(message);
      });
  };

  const handleSubmitForm = (payload) => {
    if (!payload.id) {
      payload.createBy = payload.createBy || localStorage.getItem(USER_NAME);
      instance
        .post("/v1/notice/create", payload)
        .then(() => {
          toast.success("Tạo thông báo thành công!");
          setForm(defaultForm);
          getAndSetData();
        })
        .catch((err) => {
          console.log({ ...err });
          const messerr = err.response.data.message;
          toast.error(messerr ? messerr : "Thêm thông báo thất bại!");
        });
    } else {
      payload.updateBy = payload.updateBy || localStorage.getItem(USER_NAME);
      instance
        .put("/v1/notice/update", payload)
        .then(() => {
          toast.success("Cập nhật thông báo thành công!");
          setForm(defaultForm);
          getAndSetData();
        })
        .catch((err) => {
          console.log({ ...err });
          const messerr = err.response.data.message;
          toast.error(messerr ? messerr : "Cập nhật thông báo thất bại!");
        });
    }
  };
  // console.log(form);
  const _setSearchParams = (value, field) => {
    const newState = {
      ...searchParams,
      [field]: value,
    };
    setSearchParams(newState);
  };

  return (
    <div>
      <Card title="Quản lý thông báo">
        <Row className="search-row mb-3">
          <Col span={4}>
            <Input
              placeholder="Tiêu đề/nội dung"
              onChange={(e) => _setSearchParams(e.target.value, "searchValue")}
            />
          </Col>
          <Col span={4}>
            <SelectCustome
              style={{ with: "100%" }}
              options={noticeCategories}
              value={searchParams?.type}
              placeholder="Chọn phân loại"
              onChange={(e) => _setSearchParams(e, "type")}
            />
          </Col>
          <Col span={4}>
            <DatePicker
              style={{ with: "100%" }}
              placeholder="Từ ngày"
              onChange={(e) =>
                _setSearchParams(formatDateYYYYMMDD(e), "fromDate")
              }
            />
          </Col>
          <Col span={4}>
            <DatePicker
              style={{ with: "100%" }}
              placeholder="Đến ngày"
              onChange={(e) =>
                _setSearchParams(formatDateYYYYMMDD(e), "toDate")
              }
            />
          </Col>
          <Button
            type="primary"
            onClick={getAndSetData}
            style={{ marginTop: 4 }}
          >
            Tìm kiếm
          </Button>
        </Row>
        <div style={{ textAlign: "right", marginBottom: 24 }}>
          {" "}
          {status === "DRAFT" ? (
            <Button
              type="primary"
              onClick={() =>
                setForm({
                  ...defaultForm,
                  isShowForm: !form.isShowForm,
                })
              }
            >
              Thêm mới
            </Button>
          ) : null}
        </div>

        {isLoading ? (
          <Spin size="large" className="spin-loading" />
        ) : (
          <div className="table-notice">
            <Table
              columns={columns}
              dataSource={data}
              bordered
              footer={() => (
                <div style={{ textAlign: "left" }}>Tổng {maxCounts}</div>
              )}
            />
          </div>
        )}
        {status !== "PENDING" ? (
          <NoticeForm
            toggleForm={() => setForm(defaultForm)}
            visible={form.isShowForm}
            noticeCategories={noticeCategories}
            handleSubmit={handleSubmitForm}
            initialValues={{ ...form.modalUD }}
          />
        ) : (
          <NoticeFormAppove
            toggleForm={() => setForm(defaultForm)}
            visible={form.isShowForm}
            noticeCategories={noticeCategories}
            handleSubmit={handleSubmitForm}
            initialValues={{ ...form.modalUD }}
          />
        )}
      </Card>
    </div>
  );
}
