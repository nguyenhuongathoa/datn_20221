import { Button, Form, Input, Modal } from "antd";
import React, { useEffect, useState } from "react";
import { statusNotice } from "../../../config/const";
import RichTextEditor from "../../../helpers/RichTextEditor";
import SelectCustome from "../../../helpers/SelectCustome";
import "./style.css";

export default function NoticeForm(props) {
  const handleOk = () => {
    const formValue = form.getFieldsValue(true);
    formValue.status = "DRAFT";
    props.handleSubmit(formValue);
  };
  const handleSubmit = () => {
    const formValue = form.getFieldsValue(true);
    console.log(form);
    console.log(formValue);
    formValue.status = "PENDING";
    props.handleSubmit(formValue);
  };
  const handleCancel = () => {
    props.toggleForm();
  };
  const [form] = Form.useForm();
  const onFinish = (values) => {
    console.log(values);
  };
  const onFinishFailed = () => {};
  useEffect(() => {
    console.log("render");
    form.setFieldsValue({
      ...props.initialValues,
    });
  }, [props.initialValues]);
  return (
    <div className="notice-form">
      <Modal
        title={props?.modalUD?.id ? "Cập nhật thông báo" : "Thêm mới thông báo"}
        visible={props.visible}
        onOk={handleOk}
        onCancel={handleCancel}
        width={1000}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Hủy thay đổi
          </Button>,
          <Button
            key="save"
            type="primary"
            // loading={loading}
            onClick={handleOk}
          >
            Lưu nháp
          </Button>,
          <Button key="submit" type="primary" onClick={handleSubmit}>
            trình duyệt
          </Button>,
        ]}
      >
        <Form
          form={form}
          layout="vertical"
          name="basic"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          initialValues={{
            ...props.initialValues,
          }}
        >
          <Form.Item
            label="Tiêu đề"
            name="subject"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập tiêu đề!",
              },
            ]}
            initialValue=""
          >
            <Input />
          </Form.Item>
          <div className="row">
            <div className="col-6">
              <Form.Item
                label="Phân loại"
                name="noticeCategory"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng chọn phân loại!",
                  },
                ]}
              >
                <SelectCustome
                  placeholder="Chọn phân loại"
                  options={props.noticeCategories}
                />
              </Form.Item>
            </div>
          </div>
          <Form.Item label="Nội dung" name="noticecontent">
            <RichTextEditor />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}
