import { Button, Form, Input, Modal } from "antd";
import React, { useEffect, useState } from "react";
import { ROLE, statusNotice, USER_ID } from "../../../config/const";
import RichTextEditor from "../../../helpers/RichTextEditor";
import SelectCustome from "../../../helpers/SelectCustome";
import "./style.css";

export default function NoticeFormAppove(props) {
  const handleOk = () => {
    const formValue = form.getFieldsValue(true);
    console.log(form);
    console.log(formValue);
    formValue.status = "REFUSE";
    console.log(formValue);
    props.handleSubmit(formValue);
  };
  const handleSubmit = () => {
    const formValue = form.getFieldsValue(true);
    console.log(form);
    console.log(formValue);
    formValue.status = "APPROVE";
    props.handleSubmit(formValue);
  };
  const handleCancel = () => {
    props.toggleForm();
  };
  const [form] = Form.useForm();
  const onFinish = (values) => {
    console.log(values);
  };
  const onFinishFailed = () => {};
  useEffect(() => {
    console.log("render");
    form.setFieldsValue({
      ...props.initialValues,
    });
  }, [props.initialValues]);
  const okButtonDisable = localStorage.getItem(ROLE) == "ADMIN" ? false : true;
  console.log("okButtonDisable:", okButtonDisable);
  return (
    <div className="notice-form">
      <Modal
        title={"Cập nhật thông báo"}
        visible={props.visible}
        onOk={handleOk}
        onCancel={handleCancel}
        okButtonProps={{ disabled: okButtonDisable }}
        width={1000}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Quay lại
          </Button>,
          <Button
            key="save"
            type="primary"
            // loading={loading}
            onClick={handleOk}
            disabled={okButtonDisable}
          >
            Từ chối
          </Button>,
          <Button
            key="submit"
            type="primary"
            onClick={handleSubmit}
            disabled={okButtonDisable}
          >
            Phê duyệt
          </Button>,
        ]}
      >
        <Form
          form={form}
          layout="vertical"
          name="basic"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          initialValues={{
            ...props.initialValues,
          }}
        >
          <Form.Item
            label="Tiêu đề"
            name="subject"
            disabled
            rules={[
              {
                required: true,
                message: "Vui lòng nhập tiêu đề!",
              },
            ]}
            initialValue=""
          >
            <Input disabled />
          </Form.Item>
          <div className="row">
            <div className="col-6">
              <Form.Item
                label="Phân loại"
                name="noticeCategory"
                disabled
                rules={[
                  {
                    required: true,
                    message: "Vui lòng chọn phân loại!",
                  },
                ]}
              >
                <SelectCustome
                  placeholder="Chọn phân loại"
                  options={props.noticeCategories}
                  disabled
                />
              </Form.Item>
            </div>
          </div>
          <Form.Item label="Nội dung" name="noticecontent">
            <RichTextEditor readOnly />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}
