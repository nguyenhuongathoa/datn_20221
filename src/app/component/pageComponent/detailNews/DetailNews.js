import { Button, Layout, Menu } from "antd";
import React, { useEffect, useState } from "react";
import instance from "../../../config/axiosClient";
import { Link } from "react-router-dom";
import imageicon from "../../../assets/Mai động/icon.png";
import "../homePage/homePage.css";
import { formatDateDDMMYYYY } from "../../../helpers/utlis";

const { Content } = Layout;
function DetailNewsComponent(props) {
  const [data, setData] = useState({});
  useEffect(() => {
    instance
      .get(`/v1/notice/new/${props?.match?.params?.id || ""}`)
      .then((res) => {
        const dataDetail = res?.data?.data || {};
        setData(dataDetail);
      });
  }, []);
  return (
    <Content className="image-slide content-home-page">
      <div className="header">
        <div className="header-left">
          <img width={140} src={imageicon}></img>
          <div>
            <h1>QUẬN HOÀNG MAI</h1>
            <h2>THÔNG TIN PHƯỜNG MAI ĐỘNG</h2>
          </div>
        </div>
        <Link to="/login">
          <Button className="btn-login">Đăng nhập</Button>
        </Link>
        {/* </Space> */}
      </div>
      <Content style={{ margin: "0 16px" }}>
        <div style={{ margin: "100px 175px" }}>
          <h2>{data?.subject}</h2>
          <p>{formatDateDDMMYYYY(data?.updateDate)}</p>
        </div>

        <div
          dangerouslySetInnerHTML={{ __html: data?.noticecontent || "" }}
          style={{ margin: "100px 175px" }}
        ></div>
      </Content>
      <div style={{ textAlign: "center", fontWeight: 600, marginTop: 10 }}>
        <div>Phường Mai Động</div>
        <div style={{ marginBottom: 0 }}>
          13 Đ. Lĩnh Nam, Mai Động, Hoàng Mai, Hà Nội
        </div>
      </div>
    </Content>
  );
}

export default DetailNewsComponent;
