import React, { useEffect, useState } from "react";
import API from "../../../config/api";
import { toast } from "react-toastify";
import instance from "../../../config/axiosClient";
import { GET_LIST_ERROR, USER_NAME } from "../../../config/const";
import { formatDateDDMMYYYY, normalizeToOptions } from "../../../helpers/utlis";
import { DeleteOutlined } from "@ant-design/icons";
import { Button, Card, Spin, Table, Popconfirm } from "antd";
import UserForm from "../userManagement/form";
import ReactApexChart from "react-apexcharts";
import "./style.css";
import CensusForm from "./form";
import moment from "moment";

const pageSize = 20;
const defaultForm = {
  isShowForm: false,
  modalUD: {
    censusId: null,
    totalCitizen: 0,
    maleNumber: 0,
    femaleNumber: 0,
    youthNumber: 0,
    middleNumber: 0,
    oldNumber: 0,
  },
};
export default function CensusManagement() {
  const [census, setCensus] = useState([]);
  const [pagination, setPagination] = useState({
    pageSize: 20,
    pageIndex: 1,
    totalRecord: 0,
  });
  const [form, setForm] = useState(defaultForm);
  const [isLoading, setIsLoading] = useState(false);
  const [caculates, setCaculates] = useState([]);

  const getAndSetData = () => {
    setIsLoading(true);
    instance
      .get(API.census.getAll)
      .then((res) => {
        // console.log(res);
        const data = res?.data?.data;
        // console.log(data);
        setCensus(data);
        setIsLoading(false);
      })
      .catch(() => {
        toast.error(GET_LIST_ERROR);
      });
    instance
      .get(API.census.get)
      .then((res) => {
        const data = res?.data?.data;
        setCaculates(data);
      })
      .catch(() => {
        toast.error(GET_LIST_ERROR);
      });
  };
  useEffect(() => {
    // instance.get(`${API.reference.getCategory}ROLE_TYPE`).then((res) => {
    //   const options = res?.data?.data
    //     ? normalizeToOptions(res?.data?.data, "name", "code")
    //     : [];
    //   setRoleCategories(options);
    // });
    getAndSetData();
  }, []);
  const columns = [
    {
      title: "STT",
      dataIndex: "index",
      key: "1",
    },
    {
      title: "Tổng dân số",
      dataIndex: "totalCitizen",
      key: "2",
    },
    {
      title: "Giới tính nữ",
      dataIndex: "femaleNumber",
      key: "3",
    },
    {
      title: "Giới tính name",
      dataIndex: "maleNumber",
      key: "4",
    },
    {
      title: "Tuổi từ 0-15",
      dataIndex: "youthNumber",
      key: "5",
    },
    {
      title: "Tuổi từ 16-65",
      dataIndex: "middleNumber",
      key: "6",
    },
    {
      title: "Tuổi >65",
      dataIndex: "oldNumber",
      key: "7",
    },
    {
      title: "Người tạo",
      dataIndex: "createBy",
      key: "8",
    },
    {
      title: "Ngày tạo",
      dataIndex: "createdDate",
      key: "9",
    },
    {
      title: "Tác vụ",
      dataIndex: "actions",
      key: "10",
    },
  ];
  const data = census.map((item, index) => {
    return {
      ...item,
      index: (pagination.pageIndex - 1) * pagination.pageSize + 1 + index,
      createdDate: formatDateDDMMYYYY(item.createDate),
      actions: (
        <>
          <Popconfirm
            placement="topRight"
            title="Bạn có chắc chắn muốn xóa thống kê này không?"
            onConfirm={() => onClickDelete(item.censusId)}
            okText="Yes"
            cancelText="No"
          >
            <Button>
              <DeleteOutlined />
            </Button>
          </Popconfirm>
        </>
      ),
    };
  });
  const onClickDelete = (id) => {
    instance
      .delete(`${API.census.delete}${id}`)
      .then(() => {
        toast.success("Xóa báo cáo Thống kê thành công!");
        getAndSetData();
      })
      .catch((err) => {
        const message =
          err?.response?.data?.message || "Xóa báo cáo thống kê thất bại!";
        toast.error(message);
      });
  };
  const handleSubmitForm = (payload) => {
    if (!payload.id) {
      payload.createBy = payload.createBy || localStorage.getItem(USER_NAME);
      instance
        .post(API.census.create, payload)
        .then(() => {
          toast.success("Tạo thống kê thành công!");
          setForm(defaultForm);
          getAndSetData();
        })
        .catch((err) => {
          console.log({ ...err });
          toast.error("Thêm thống kê thất bại!");
        });
    }
  };

  const seriechar = [
    {
      name: "công dân",
      data: census.map((item) => item.totalCitizen),
    },
  ];

  const serieSexchar = [
    {
      name: "Nam",
      data: census.map((item) => item.maleNumber),
    },
    {
      name: "Nữ",
      data: census.map((item) => item.femaleNumber),
    },
  ];
  const serieAgechar = [
    {
      name: "0-15 tuổi",
      data: census.map((item) => item.youthNumber),
    },
    {
      name: "16-65 tuổi",
      data: census.map((item) => item.middleNumber),
    },
    {
      name: ">65 tuổi",
      data: census.map((item) => item.oldNumber),
    },
  ];
  //const lableChar = ["Feb", "Feb", "Apr"];

  const lableChar = [
    ...census.map((item) => {
      return (
        "Tháng " +
        (moment(item.createDate).month() + 1) +
        "/" +
        moment(item.createDate).year()
      );
    }),
  ];

  // const data = census.map((item, index) => {});
  const statechar = (serieColumChar = [], labelColumChar = []) => {
    return {
      series: serieColumChar,
      options: {
        chart: {
          type: "bar",
          height: 350,
        },
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: "55%",
            endingShape: "rounded",
          },
        },
        dataLabels: {
          enabled: false,
        },
        stroke: {
          show: true,
          width: 2,
          colors: ["transparent"],
        },
        xaxis: {
          categories: labelColumChar,
        },
        yaxis: {
          title: {
            text: "(Người)",
          },
        },
        fill: {
          opacity: 1,
        },
        tooltip: {
          y: {
            formatter: function (val) {
              return val + " người";
            },
          },
        },
      },
    };
  };

  return (
    <div>
      <Card title="Quản lý thống kê">
        <div style={{ textAlign: "right", marginBottom: 24 }}>
          {" "}
          <Button
            type="primary"
            onClick={() =>
              setForm({
                ...defaultForm,
                modalUD: caculates ? caculates : defaultForm.modalUD,
                isShowForm: true,
              })
            }
          >
            Tạo thống kê
          </Button>
        </div>
        {isLoading ? (
          <Spin size="large" className="spin-loading" />
        ) : (
          <div className="table-census">
            <Table columns={columns} dataSource={data} bordered />
          </div>
        )}
        <CensusForm
          toggleForm={() => setForm(defaultForm)}
          visible={form.isShowForm}
          handleSubmit={handleSubmitForm}
          initialValues={{ ...form.modalUD }}
        />
        <div className="row">
          <div className="col-6">
            {" "}
            <div id="chart">
              <ReactApexChart
                options={statechar(seriechar, lableChar)?.options}
                series={statechar(seriechar, lableChar)?.series}
                type="bar"
                height={350}
                width={500}
              />
            </div>
            <div className="title-Char">
              <h5>Đồ thị biến động dân số</h5>{" "}
            </div>
          </div>
          <div className="col-6">
            {" "}
            <div id="chart">
              <ReactApexChart
                options={statechar(serieSexchar, lableChar)?.options}
                series={statechar(serieSexchar, lableChar)?.series}
                type="bar"
                height={350}
                width={500}
              />
            </div>
            <div className="title-Char">
              <h5>Đồ thị biến động giới tính</h5>{" "}
            </div>
          </div>
        </div>
        <div>
          {" "}
          <div id="chart">
            <ReactApexChart
              options={statechar(serieAgechar, lableChar)?.options}
              series={statechar(serieAgechar, lableChar)?.series}
              type="bar"
              height={350}
            />
          </div>
          <div className="title-Char">
            <h5>Đồ thị biến động độ tuổi</h5>{" "}
          </div>
        </div>
      </Card>
    </div>
  );
}
