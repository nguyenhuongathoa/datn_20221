import { Form, Input, Modal, Select } from "antd";
import React, { useEffect, useState } from "react";
import SelectCustome from "../../../helpers/SelectCustome";
import "./style.css";

export default function CensusForm(props) {
  const handleOk = () => {
    const formValue = form.getFieldsValue(true);
    console.log(form);
    console.log(formValue);
    props.handleSubmit(formValue);
  };
  const handleCancel = () => {
    props.toggleForm();
  };
  const [form] = Form.useForm();
  const onFinish = (values) => {
    console.log(values);
  };
  const onFinishFailed = () => {};
  useEffect(() => {
    console.log("render");
    form.setFieldsValue({
      ...props.initialValues,
    });
  }, [props.initialValues]);
  return (
    <div className="user-form">
      <Modal
        title={"Tạo thống kê dân số"}
        visible={props.visible}
        onOk={handleOk}
        onCancel={handleCancel}
        style={{ width: "520px !important" }}
      >
        <Form
          form={form}
          layout="vertical"
          name="basic"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item label="Tổng dân số">
            <Form.Item name="totalCitizen" noStyle>
              <Input disabled />
            </Form.Item>
          </Form.Item>

          <Form.Item label="Tổng số nam">
            <Form.Item name="maleNumber" noStyle>
              <Input disabled />
            </Form.Item>
          </Form.Item>

          <Form.Item label="Tổng số nữ">
            <Form.Item name="femaleNumber" noStyle>
              <Input disabled />
            </Form.Item>
          </Form.Item>

          <Form.Item label="Tuổi từ 0-15">
            <Form.Item name="youthNumber" noStyle>
              <Input disabled />
            </Form.Item>
          </Form.Item>

          <Form.Item label="Tuổi từ 16-65">
            <Form.Item name="middleNumber" noStyle>
              <Input disabled />
            </Form.Item>
          </Form.Item>

          <Form.Item label="Tuổi > 65">
            <Form.Item name="oldNumber" noStyle>
              <Input disabled />
            </Form.Item>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}
