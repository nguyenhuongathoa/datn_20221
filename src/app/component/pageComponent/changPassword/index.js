import { Button, Input, Form } from "antd";
import React, { useEffect } from "react";
import { toast } from "react-toastify";
import API from "../../../config/api";
import instance from "../../../config/axiosClient";
import { CITIZEN_ID, USER_NAME } from "../../../config/const";

export default function ChangPassWord() {
  const [form] = Form.useForm();
  const defaultForm = {
    citizenId: localStorage.getItem(CITIZEN_ID),
    userName: localStorage.getItem(USER_NAME),
    userPassword: "",
    userPasswordNew: "",
    userPasswordConfirm: "",
  };

  useEffect(() => {
    // console.log("render");
    // form.setFieldsValue({
    //   ...defaultForm,
    // });
    // console.log("defaultForm", defaultForm);
  }, [defaultForm]);

  const onFinishFailed = (values) => {};
  const onFinish = (values) => {
    console.log(values);
    const formValue = form.getFieldsValue(true);
    instance
      .put(API.user.changePassword, formValue)
      .then(() => {
        toast.success("Đổi mật khẩu thành công!");
      })
      .catch((err) => {
        console.log({ ...err });
        const messerr = err.response.data.message;
        toast.error(messerr ? messerr : "Đổi mật khẩu thất bại!");
      });
  };
  console.log("form", form.getFieldsValue());

  return (
    <Form
      form={form}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={{ ...defaultForm }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item label="Tên đăng nhập" name="userName">
        <Input disabled />
      </Form.Item>

      <Form.Item
        label="Mật khẩu"
        name="userPassword"
        rules={[{ required: true, message: "Vui lòng nhập mật khẩu cũ!" }]}
      >
        <Input.Password />
      </Form.Item>
      <Form.Item
        label="Mật khẩu mới"
        name="userPasswordNew"
        rules={[{ required: true, message: "Vui lòng nhập mật khẩu mới!" }]}
      >
        <Input.Password />
      </Form.Item>
      <Form.Item
        label="Xác nhận mật khẩu"
        name="userPasswordConfirm"
        rules={[
          { required: true, message: "Vui lòng nhập Xác nhận lại mật khẩu!" },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (!value || getFieldValue("userPasswordNew") === value) {
                return Promise.resolve();
              }
              return Promise.reject(
                new Error("Mật khẩu xác nhận không khớp mật khẩu mới!")
              );
            },
          }),
        ]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
        <Button type="primary" htmlType="submit">
          Lưu
        </Button>
      </Form.Item>
    </Form>
  );
}
