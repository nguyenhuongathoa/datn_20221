import { Layout } from "antd";
import React from "react";
import "./footer.css";
const { Footer } = Layout;
function FooterCustom() {
  return (
    <Footer style={{ textAlign: "center" }}>
      <div>
        <p> Phường Mai Động</p>
        <p style={{ marginBottom: 0 }}>
          Đ/c: 13 Đ. Lĩnh Nam, Mai Động, Hoàng Mai, Hà Nội
        </p>
      </div>
    </Footer>
  );
}

export default FooterCustom;
