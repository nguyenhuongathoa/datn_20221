import { Layout } from "antd";
import "antd/dist/antd.css";
import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
//import AppRouter from "./app/config/routes.js";
import { BrowserRouter } from "react-router-dom";
import "./style.css";
import ContentCustome from "./content";
import FooterCustom from "./footer/Footer";
import HeaderCustom from "./header/Header";
import Sidebar from "./sidebar/Sidebar";
import { Route, Switch } from "react-router-dom";
import { ROUTER_CONST } from "../../config/const";
import ContentDetail from "../layout/content/contentDetail";
function LayoutCustome() {
  return (
    //  const { collapsed } = this.state;
    <BrowserRouter>
      <Layout style={{ minHeight: "100vh" }}>
        <Sidebar />
        <Layout className="site-layout">
          <HeaderCustom />
          <Switch>
            <Route path={ROUTER_CONST.home} component={ContentCustome} exact />
            {/* <Route path="*" component={NotFoundPage} /> */}
            <Route path="/:id" component={ContentDetail} />
          </Switch>
          <FooterCustom />
        </Layout>
      </Layout>
    </BrowserRouter>
  );
}

export default LayoutCustome;
