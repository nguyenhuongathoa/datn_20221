import React from "react";

export default function ContentAdmin(props) {
  return <div style={{ minHeight: "78vh", margin: 10 }}>{props.children}</div>;
}
