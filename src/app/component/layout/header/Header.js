import { Layout } from "antd";
import React from "react";
import { FULL_NAME, ROLE_TYPE } from "../../../config/const";
import "./header.css";
const { Header } = Layout;
function HeaderCustom() {
  return (
    <Header className="site-layout-background" style={{ padding: 0 }}>
      <div className="row site-layout-title">
        <div className="col-6">THÔNG TIN PHƯỜNG MAI ĐỘNG</div>
        <div className="col-6 text-end">
          <h6>
            {localStorage.getItem(FULL_NAME)}- {localStorage.getItem(ROLE_TYPE)}
          </h6>
        </div>
      </div>
    </Header>
  );
} //fs-6

export default HeaderCustom;
