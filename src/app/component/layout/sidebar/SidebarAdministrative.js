import { Layout, Menu } from "antd";
import {
  DesktopOutlined,
  LogoutOutlined,
  KeyOutlined,
} from "@ant-design/icons";
import React, { useState } from "react";
import { ROUTER_CONST } from "../../../config/const";
import { Link } from "react-router-dom";
import "./sidebar.css";
const { Sider } = Layout;
const { SubMenu } = Menu;
function SidebarAdministrative({ dataSidebar }) {
  const [collapsed, setCollapsed] = useState(false);

  const onCollapse = (collapsed) => {
    setCollapsed(collapsed);
  };
  //dataSidebar have title and value
  return (
    <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
      <div className="logo" />
      <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
        <SubMenu
          key="sub1"
          icon={<DesktopOutlined />}
          title="Quản lý thông báo"
        >
          <Menu.Item key="1.1">
            {" "}
            <Link to={`${ROUTER_CONST.administrativeNotice}/DRAFT`}>
              Bản nháp
            </Link>
          </Menu.Item>
          <Menu.Item key="1.2">
            {" "}
            <Link to={`${ROUTER_CONST.administrativeNotice}/PENDING`}>
              Bản chờ duyệt
            </Link>
          </Menu.Item>
          <Menu.Item key="1.3">
            {" "}
            <Link to={`${ROUTER_CONST.administrativeNotice}/REFUSE`}>
              Bản đã hủy
            </Link>
          </Menu.Item>
          <Menu.Item key="1.4">
            {" "}
            <Link to={`${ROUTER_CONST.administrativeNotice}/APPROVE`}>
              Bản đã duyệt
            </Link>
          </Menu.Item>
        </SubMenu>
        <Menu.Item key="2" icon={<LogoutOutlined />}>
          <Link to={ROUTER_CONST.home}> Đăng xuất</Link>
        </Menu.Item>
        <Menu.Item key="3" icon={<KeyOutlined />}>
          <Link to={ROUTER_CONST.administrativeChangePassword}>
            {" "}
            Đổi mật khẩu
          </Link>
        </Menu.Item>
      </Menu>
    </Sider>
  );
}

export default SidebarAdministrative;
