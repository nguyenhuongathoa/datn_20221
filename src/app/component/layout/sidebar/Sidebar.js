import { Layout, Menu } from "antd";
import {
  DesktopOutlined,
  LogoutOutlined,
  PieChartOutlined,
  TeamOutlined,
  UserOutlined,
  KeyOutlined,
  UserAddOutlined,
  AreaChartOutlined,
} from "@ant-design/icons";
import React, { useState } from "react";
import { ROUTER_CONST } from "../../../config/const";
import { Link } from "react-router-dom";
import "./sidebar.css";
const { Sider } = Layout;
const { SubMenu } = Menu;
function Sidebar({ dataSidebar }) {
  const [collapsed, setCollapsed] = useState(false);

  const onCollapse = (collapsed) => {
    setCollapsed(collapsed);
  };
  //dataSidebar have title and value
  return (
    <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
      <div className="logo" />
      <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
        <SubMenu
          key="sub1"
          icon={<DesktopOutlined />}
          title="Quản lý thông báo"
        >
          <Menu.Item key="1.1">
            {" "}
            <Link to={`${ROUTER_CONST.notice}/DRAFT`}>Bản nháp</Link>
          </Menu.Item>
          <Menu.Item key="1.2">
            {" "}
            <Link to={`${ROUTER_CONST.notice}/PENDING`}>Bản chờ duyệt</Link>
          </Menu.Item>
          <Menu.Item key="1.3">
            {" "}
            <Link to={`${ROUTER_CONST.notice}/REFUSE`}>Bản đã hủy</Link>
          </Menu.Item>
          <Menu.Item key="1.4">
            {" "}
            <Link to={`${ROUTER_CONST.notice}/APPROVE`}>Bản đã duyệt</Link>
          </Menu.Item>
        </SubMenu>
        <Menu.Item key="2" icon={<UserOutlined />}>
          <Link to={ROUTER_CONST.citizenManagement}> Quản lý công dân</Link>
        </Menu.Item>
        <Menu.Item key="3" icon={<TeamOutlined />}>
          <Link to={ROUTER_CONST.householdManagement}> Quản lý hộ dân</Link>
        </Menu.Item>
        <Menu.Item key="5" icon={<UserAddOutlined />}>
          <Link to={ROUTER_CONST.userManagement}> Quản lý tài khoản</Link>
        </Menu.Item>
        <Menu.Item key="4" icon={<PieChartOutlined />}>
          <Link to={ROUTER_CONST.totalManagement}> Tổng quát</Link>
        </Menu.Item>
        <Menu.Item key="6" icon={<AreaChartOutlined />}>
          <Link to={ROUTER_CONST.censusManagement}> Quản lý thống kê</Link>
        </Menu.Item>
        <Menu.Item key="7" icon={<KeyOutlined />}>
          <Link to={ROUTER_CONST.changePassword}> Đổi mật khẩu</Link>
        </Menu.Item>
        <Menu.Item key="8" icon={<LogoutOutlined />}>
          <Link to={ROUTER_CONST.home}> Đăng xuất</Link>
        </Menu.Item>
      </Menu>
    </Sider>
  );
}

export default Sidebar;
