import { Layout, Menu } from "antd";
import {
  UserOutlined,
  LogoutOutlined,
  TeamOutlined,
  KeyOutlined,
  PieChartOutlined,
  AreaChartOutlined,
} from "@ant-design/icons";
import React, { useState } from "react";
import { ROUTER_CONST } from "../../../config/const";
import { Link } from "react-router-dom";
import "./sidebar.css";
const { Sider } = Layout;
const { SubMenu } = Menu;
function SidebarPolice({ dataSidebar }) {
  const [collapsed, setCollapsed] = useState(false);

  const onCollapse = (collapsed) => {
    setCollapsed(collapsed);
  };
  //dataSidebar have title and value
  return (
    <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
      <div className="logo" />
      <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
        <Menu.Item key="1" icon={<UserOutlined />}>
          <Link to={ROUTER_CONST.policeHouseholdManagement}>
            Quản lý hộ gia đình
          </Link>
        </Menu.Item>
        <Menu.Item key="2" icon={<TeamOutlined />}>
          <Link to={ROUTER_CONST.policeCitizenManagement}>
            {" "}
            Quản lý công dân
          </Link>
        </Menu.Item>
        <Menu.Item key="3" icon={<PieChartOutlined />}>
          <Link to={ROUTER_CONST.policeTotalManagement}> Tổng quát</Link>
        </Menu.Item>
        <Menu.Item key="4" icon={<AreaChartOutlined />}>
          <Link to={ROUTER_CONST.policeCensusManagement}>
            {" "}
            Quản lý thống kê
          </Link>
        </Menu.Item>
        <Menu.Item key="5" icon={<KeyOutlined />}>
          <Link to={ROUTER_CONST.policeChangePassword}> Đổi mật khẩu</Link>
        </Menu.Item>
        <Menu.Item key="6" icon={<LogoutOutlined />}>
          <Link to={ROUTER_CONST.home}> Đăng xuất</Link>
        </Menu.Item>
      </Menu>
    </Sider>
  );
}

export default SidebarPolice;
