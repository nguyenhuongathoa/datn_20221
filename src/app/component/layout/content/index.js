import { Layout, Breadcrumb, Menu } from "antd";
import React, { useEffect, useState } from "react";
import "./style.css";
import { data } from "./data.js";
import { Link } from "react-router-dom";

const { Content } = Layout;
const SubMenu = Menu;
function ContentCustome() {
  const [current, setcurrent] = useState("NEW");
  const [subList, setSubList] = useState([]);

  const handleClick = (e) => {
    setcurrent(e.key);
  };

  useEffect(() => {
    const subListFilter = data.filter((item) => {
      return item.noticeCategory == current;
    });
    setSubList(subListFilter);
  }, [current]);
  return (
    <Content style={{ margin: "0 16px" }}>
      <Menu onClick={handleClick} selectedKeys={[current]} mode="horizontal">
        <Menu.Item key="NEW">TIN TỨC</Menu.Item>
        <Menu.Item key="ACTIVITY">HOẠT ĐỘNG</Menu.Item>
        <Menu.Item key="POLICY">CHÍNH SÁCH</Menu.Item>
        <Menu.Item key="TOTAL">TỔNG QUÁT</Menu.Item>
      </Menu>
      <div
        className="site-layout-background"
        style={{ padding: 24, minHeight: 566 }}
      >
        {subList.map((newa) => {
          return (
            <div class="item">
              <div class="item-content item-left date">
                <p class="month">{newa.month}</p>
                <p class="date"> {newa.day}</p>
              </div>
              <div class="item-content item-right">
                <Link to={`/${newa.id}`}>
                  <p class="title">
                    <b>{newa.noticeCategory}</b> {newa.subject}
                  </p>
                  <p class="datetime">{newa.createDate}</p>
                </Link>
              </div>
            </div>
          );
        })}
      </div>
    </Content>
  );
}

export default ContentCustome;
