export const fakeData = {
  id: 1,
  title: "new",
  content: (
    <div>
      <h2 style='color: rgb(0, 0, 0); margin: 1em 0px 0.25em; padding: 0px; overflow: hidden; border-bottom: 1px solid rgb(162, 169, 177); font-size: 1.5em; font-weight: normal; font-family: "Linux Libertine", "Palatino Linotype", "EB Garamond", "Times New Roman", Times, serif; line-height: 1.3; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;'>
        <span class="mw-headline">Địa l&yacute;</span>
      </h2>
      <p style="margin: 0.5em 0px; color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
        Địa giới h&agrave;nh ch&iacute;nh của phường n&agrave;y như sau:
      </p>
      <ul style='list-style-image: url("/w/skins/Vector/resources/common/images/bullet-icon.svg?d4515"); margin: 0.3em 0px 0px 1.6em; padding: 0px; color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;'>
        <li style="margin-bottom: 0.1em;">
          Nam gi&aacute;p phường Ho&agrave;ng Văn Thụ
        </li>
        <li style="margin-bottom: 0.1em;">
          Bắc gi&aacute;p phường Minh Khai, quận Hai B&agrave; Trưng
        </li>
        <li style="margin-bottom: 0.1em;">
          Đ&ocirc;ng gi&aacute;p phường Vĩnh Hưng
        </li>
        <li style="margin-bottom: 0.1em;">
          T&acirc;y gi&aacute;p phường Ho&agrave;ng Văn Thụ.
        </li>
      </ul>
      <h2 style='color: rgb(0, 0, 0); margin: 1em 0px 0.25em; padding: 0px; overflow: hidden; border-bottom: 1px solid rgb(162, 169, 177); font-size: 1.5em; font-weight: normal; font-family: "Linux Libertine", "Palatino Linotype", "EB Garamond", "Times New Roman", Times, serif; line-height: 1.3; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;'>
        <span class="mw-headline">Lịch sử</span>
      </h2>
      <p style="margin: 0.5em 0px; color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
        Mai Động c&oacute; t&ecirc;n cổ l&agrave; My Động. Năm 1979, khi nạo
        v&eacute;t s&ocirc;ng Kim Ngưu, những c&ocirc;ng cụ bằng đ&aacute;
        c&oacute; ni&ecirc;n đại khoảng từ 3.500 năm đến 4.000 năm đ&atilde;
        được ph&aacute;t hiện.
      </p>
      <p style="margin: 0.5em 0px; color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
        V&agrave;o những năm đầu C&ocirc;ng nguy&ecirc;n, Tam Trinh, người quận
        Cửu Ch&acirc;n, đến Mai Động mở trường dạy văn v&agrave; v&otilde;. Năm
        40, khi Hai B&agrave; Trưng phất cờ khởi nghĩa, Tam Trinh đem 3.000
        tr&aacute;ng đinh l&ecirc;n H&aacute;t M&ocirc;n tụ nghĩa.
      </p>
      <p style="margin: 0.5em 0px; color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
        Đầu thế kỷ XV, Mai Động l&agrave; nơi diễn ra một trận chiến giữa
        qu&acirc;n Minh do Vương Th&ocirc;ng chỉ huy v&agrave; nghĩa qu&acirc;n
        của L&ecirc; Lợi do Th&aacute;i gi&aacute;m L&ecirc; Nguyễn chỉ huy.
      </p>
      <h3 style="color: rgb(0, 0, 0); margin: 0.3em 0px 0px; padding-top: 0.5em; padding-bottom: 0px; overflow: hidden; font-size: 1.2em; line-height: 1.6; font-weight: bold; font-family: sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
        <span class="mw-headline">H&agrave;nh ch&iacute;nh</span>
      </h3>
      <p style="margin: 0.5em 0px; color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
        Phường Mai Động được th&agrave;nh lập ng&agrave;y 13 th&aacute;ng 10 năm{" "}
        <a
          href="https://vi.wikipedia.org/wiki/1982"
          style="text-decoration: none; color: rgb(6, 69, 173); background: none;"
          title="1982"
        ></a>
        1982 &nbsp;tr&ecirc;n cơ sở t&aacute;ch th&ocirc;n Mai Động v&agrave;
        x&oacute;m Mơ T&aacute;o của x&atilde; Ho&agrave;ng Văn Thụ thuộc huyện
        Thanh Tr&igrave;{" "}
        <a
          href="https://vi.wikipedia.org/wiki/Thanh_Tr%C3%AC"
          style="text-decoration: none; color: rgb(6, 69, 173); background: none;"
          title="Thanh Trì"
        ></a>
        cũ, l&uacute;c đầu phường thuộc quận Hai B&agrave; Trưng.
      </p>
      <p style="margin: 0.5em 0px; color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
        Từ{" "}
        <a
          href="https://vi.wikipedia.org/wiki/1_th%C3%A1ng_1"
          style="text-decoration: none; color: rgb(6, 69, 173); background: none;"
          title="1 tháng 1"
        ></a>
        1 th&aacute;ng 1 năm{" "}
        <span style="color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">
          2004
        </span>
        , phường Mai Động chuyển sang trực thuộc quận Ho&agrave;ng Mai.
      </p>
      <h2 style='color: rgb(0, 0, 0); margin: 1em 0px 0.25em; padding: 0px; overflow: hidden; border-bottom: 1px solid rgb(162, 169, 177); font-size: 1.5em; font-weight: normal; font-family: "Linux Libertine", "Palatino Linotype", "EB Garamond", "Times New Roman", Times, serif; line-height: 1.3; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;'>
        <span class="mw-headline">Đ&igrave;nh Mai Động</span>
      </h2>
      <p style="margin: 0.5em 0px; color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
        Đ&igrave;nh Mai Động nằm ở ng&otilde; 254 Minh Khai, phường Mai Động,
        quận Ho&agrave;ng Mai, H&agrave; Nội.
      </p>
      <p style="margin: 0.5em 0px; color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
        Đ&igrave;nh Mai Động thờ Đức Th&aacute;nh{" "}
        <a
          href="https://vi.wikipedia.org/wiki/Tam_Trinh"
          style="text-decoration: none; color: rgb(6, 69, 173); background: none;"
          title="Tam Trinh"
        ></a>
        Tam Trinh. Ng&agrave;i l&agrave; tướng của Hai B&agrave; Trưng v&agrave;
        đ&atilde; lập c&ocirc;ng t&iacute;ch lớn. Ng&agrave;i cũng l&agrave;
        người đ&atilde; dạy chữ v&agrave; truyền nghề l&agrave;m đậu phụ cho
        người d&acirc;n ở đ&acirc;y. Ng&agrave;i l&agrave;{" "}
        <a
          href="https://vi.wikipedia.org/wiki/Th%C3%A0nh_ho%C3%A0ng"
          style="text-decoration: none; color: rgb(6, 69, 173); background: none;"
          title="Thành hoàng"
        ></a>
        Th&agrave;nh Ho&agrave;n l&agrave;ng Mai Động, tiền th&acirc;n của
        phường Mai Động b&acirc;y giờ. Hiện tại trong đ&igrave;nh c&ograve;n lưu
        giữ 5 bia đ&aacute; c&oacute; nội dung về lịch sử v&ugrave;ng đất Mai
        Động v&agrave; c&aacute;c nh&acirc;n vật li&ecirc;n quan.
      </p>
      <p style="margin: 0.5em 0px; color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
        Về mặt kiến tr&uacute;c, &quot;nghi m&ocirc;n gồm 4 trụ biểu nh&igrave;n
        ra một gốc đa to v&agrave; hồ nhỏ h&igrave;nh chữ nhật. Ngo&agrave;i
        cổng c&oacute; tượng đ&ocirc;i voi chầu, mới thay cho sư tử đ&aacute;.
        To&agrave; tiền tế rộng 5 gian, cửa bức b&agrave;n, thềm cao 5 bậc, đầu
        h&agrave;ng hi&ecirc;n c&oacute; tượng hai vị Hộ ph&aacute;p đối diện
        nhau. Tiếp theo l&agrave; thi&ecirc;u hương 6 gian nối với hậu cung. Hai
        b&ecirc;n s&acirc;n c&oacute; nh&agrave; tả, hữu mạc 3 gian, cũng cửa
        bức b&agrave;n&quot;.
      </p>
      <p style="margin: 0.5em 0px; color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
        Đ&igrave;nh Mai Động l&agrave; di t&iacute;ch lịch sử kiến tr&uacute;c
        quốc gia.
      </p>
      <h2 style='color: rgb(0, 0, 0); margin: 1em 0px 0.25em; padding: 0px; overflow: hidden; border-bottom: 1px solid rgb(162, 169, 177); font-size: 1.5em; font-weight: normal; font-family: "Linux Libertine", "Palatino Linotype", "EB Garamond", "Times New Roman", Times, serif; line-height: 1.3; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;'>
        <span class="mw-headline">Ngh&egrave; Mai Động</span>
      </h2>
      <p style="margin: 0.5em 0px; color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
        Ngh&egrave; Mai Động l&agrave; nơi ở cũ của Đức Th&aacute;nh Tam Trinh,
        ph&iacute;a trước c&oacute; giếng Ngọc. Ngh&egrave; được dựng lại
        v&agrave;o năm Duy T&acirc;n thứ 10 (1916) với tiền tế 5 gian v&agrave;
        hậu cung 2 gian, quay hướng t&acirc;y-bắc, bố cục h&igrave;nh &ldquo;chữ
        Tam&rdquo;.
      </p>
      <p style="margin: 0.5em 0px; color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
        Ngh&egrave; Mai Động l&agrave; di t&iacute;ch lịch sử kiến tr&uacute;c
        quốc gia.
      </p>
      <h2 style='color: rgb(0, 0, 0); margin: 1em 0px 0.25em; padding: 0px; overflow: hidden; border-bottom: 1px solid rgb(162, 169, 177); font-size: 1.5em; font-weight: normal; font-family: "Linux Libertine", "Palatino Linotype", "EB Garamond", "Times New Roman", Times, serif; line-height: 1.3; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;'>
        <span class="mw-headline">Ch&ugrave;a Mai Động</span>
      </h2>
      <p style="margin: 0.5em 0px; color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
        Ch&ugrave;a Mai động nằm cạnh ngh&egrave; Mai Động. Trong ch&ugrave;a
        hiện lưu được một tấm bia ni&ecirc;n hiệu Vĩnh Trị thứ 5 (1680), một pho
        tượng của quận ch&uacute;a Trịnh Thị Ngọc Sanh v&agrave; của người em
        l&agrave; Trịnh Thị Ngọc Nhị, ngo&agrave;i ra cũng đ&atilde; x&acirc;y
        th&ecirc;m Điện Mẫu.
      </p>
      <h2 style='color: rgb(0, 0, 0); margin: 1em 0px 0.25em; padding: 0px; overflow: hidden; border-bottom: 1px solid rgb(162, 169, 177); font-size: 1.5em; font-weight: normal; font-family: "Linux Libertine", "Palatino Linotype", "EB Garamond", "Times New Roman", Times, serif; line-height: 1.3; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;'>
        <span class="mw-headline">Hội vật l&agrave;ng Mai Động</span>
      </h2>
      <p style="margin: 0.5em 0px; color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
        Hội vật l&agrave;ng Mai Động được tổ chức từ m&ugrave;ng 4 đến
        m&ugrave;ng 7 Tết nguy&ecirc;n Đ&aacute;n(hoặc m&ugrave;ng 6 theo một số
        nguồn tin
        <sup
          class="reference"
          style="line-height: 1em; font-size: 11.2px; white-space: nowrap; unicode-bidi: isolate; font-weight: normal; font-style: normal;"
        >
          <a
            href="https://vi.wikipedia.org/wiki/Mai_%C4%90%E1%BB%99ng_(ph%C6%B0%E1%BB%9Dng)#cite_note-:5-6"
            style="text-decoration: none; color: rgb(6, 69, 173); background: none; white-space: nowrap;"
          >
            ]
          </a>
        </sup>
        ). Hội vật quy tụ nhiều lứa tuổi: từ c&aacute;c b&ocirc; l&atilde;o,
        trung ni&ecirc;n, thanh ni&ecirc;n đến c&aacute;c ch&aacute;u thiếu
        ni&ecirc;n nhi đồng, tới từ c&aacute;c l&ograve; vật tr&ecirc;n cả nước.
        C&aacute;c hoạt động đặc trưng l&agrave;{" "}
        <em>
          đ&aacute;nh trống vật, thực hiện nghi lễ &quot;xe đ&agrave;i&quot; hay
          &quot;M&uacute;a Hạc&quot;, người chiến thắng d&acirc;ng hương lễ
          th&aacute;nh
        </em>
        . Người thắng cuộc nhận được một phần tiền thưởng từ đ&oacute;ng
        g&oacute;p của người d&acirc;n trong l&agrave;ng.
      </p>
      <p style="margin: 0.5em 0px; color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
        Hội vật bắt đầu sau lễ rước th&aacute;nh kiệu tưởng nhớ Đức Th&aacute;nh
        Tam Trinh.
      </p>
      <p style="margin: 0.5em 0px; color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
        Đ&acirc;y cũng l&agrave; một trong số c&aacute;c nguồn đ&ocirc; vật cho
        đội tuyển quốc gia.
      </p>
    </div>
  ),
};
