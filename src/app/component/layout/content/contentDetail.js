import { Layout, Menu } from "antd";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { data } from "./data.js";
import "./style.css";

const { Content } = Layout;
const SubMenu = Menu;
function ContentDetail() {
  const [current, setcurrent] = useState("NEW");
  const [subList, setSubList] = useState([]);
  const [contentNoti, setContentNoti] = useState();
  const paramUrl = useParams();
  const handleClick = (e) => {
    setcurrent(e.key);
  };

  useEffect(() => {
    const notiDetail = data.find((item) => {
      console.log(
        item.noticeCategory == paramUrl.id,
        "item.noticeCategory == id",
        item
      );
      return item.noticeCategory == paramUrl.id;
    });
    console.log("notiDetail==>", notiDetail);
    setContentNoti(notiDetail);
  }, [paramUrl]);

  useEffect(() => {
    const subListFilter = data.filter((item) => {
      return item.noticeCategory == current;
    });
    setSubList(subListFilter);
  }, [current]);
  return (
    <Content style={{ margin: "0 16px" }}>
      <Menu onClick={handleClick} selectedKeys={[current]} mode="horizontal">
        <Menu.Item key="NEW">TIN TỨC</Menu.Item>
        <Menu.Item key="ACTIVITY">HOẠT ĐỘNG</Menu.Item>
        <Menu.Item key="POLICY">CHÍNH SÁCH</Menu.Item>
        <Menu.Item key="TOTAL">TỔNG QUÁT</Menu.Item>
      </Menu>
      <div
        className="site-layout-background"
        dangerouslySetInnerHTML={{ __html: contentNoti?.noticecontent }}
      ></div>
    </Content>
  );
}

export default ContentDetail;
