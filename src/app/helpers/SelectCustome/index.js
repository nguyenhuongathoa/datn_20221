import { Form, Select } from "antd";
import React from "react";

const { Option } = Select;

export default function SelectCustome(props) {
  return (
    <Select
      onChange={props.onChange}
      showSearch
      filterOption={(input, option) =>
        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
      }
      {...props}
    >
      {props.options.map((item, index) => {
        return (
          <Option value={item.value} key={index}>
            {item.label}
          </Option>
        );
      })}
    </Select>
  );
}
