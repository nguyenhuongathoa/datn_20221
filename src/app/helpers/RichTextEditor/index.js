import React from "react";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import "./style.css";

export default function RichTextEditor(props) {
  return <ReactQuill theme="snow" {...props} value={props.value || ""} />;
}
