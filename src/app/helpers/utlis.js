import moment from "moment";
export const formatDateDDMMYYYY = (value) => {
  return value ? moment(value).format("DD/MM/YYYY") : null;
};
export const formatDateYYYYMMDD = (value) => {
  return value ? moment(value).format("YYYY-MM-DD") : null;
};

export const normalizeToOptions = (
  options,
  fieldLabelOrFunctionGetLabel,
  fieldValue = "id"
) => {
  if (!options || !options.length) return [];
  const isFunction = typeof fieldLabelOrFunctionGetLabel === "function";
  const isString = typeof fieldLabelOrFunctionGetLabel === "string";
  if (!isFunction && !isString) return [];
  return options.map((item) => ({
    ...item,
    value: item[fieldValue],
    label: isFunction
      ? fieldLabelOrFunctionGetLabel(item)
      : item[fieldLabelOrFunctionGetLabel],
  }));
};
