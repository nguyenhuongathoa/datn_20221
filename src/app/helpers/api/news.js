import API from "../../config/api";
import instance from "../../config/axiosClient";

export function getListNews() {
  return instance.get(API.news.getList);
}

export const createNew = (payload) => {
  return instance.post();
};
