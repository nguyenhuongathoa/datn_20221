import React, { Fragment, Suspense } from "react";
import { Route, Switch } from "react-router-dom";
import NotFoundPage from "../page/error";
// import HomePage from "../page/home";
import { ROUTER_CONST } from "./const";
import { Layout } from "antd";
import Sidebar from "../component/layout/sidebar/Sidebar";
import FooterCustom from "../component/layout/footer/Footer";
//import NewsPage from "../page/news";
//import DetailNewsPage from "../page/detailNews";
import HeaderCustom from "../component/layout/header/Header";
import NoticeManagement from "../component/pageComponent/noticeManagement";
import ContentAdmin from "../component/layout/contentAdmin";
import HouseholdManagement from "../component/pageComponent/householdManagement";
import CitizenManagement from "../component/pageComponent/citizenManagement";
import TotalManagement from "../component/pageComponent/totalManagement";
import UserManagement from "../component/pageComponent/userManagement";
import ChangPassWord from "../component/pageComponent/changPassword";
import CensusManagement from "../component/pageComponent/censusManagement";

const AppRouter = () => {
  const loader = <div className="example">Loading...</div>;

  return (
    <Fragment>
      <Suspense fallback={loader}>
        <Layout style={{ minHeight: "100vh" }}>
          <Sidebar />
          <Layout className="site-layout">
            <HeaderCustom />
            <ContentAdmin>
              <Switch>
                {/* <Route path={ROUTER_CONST.home} component={HomePage} exact /> */}

                {/* <Route path={ROUTER_CONST.news} component={NewsPage} /> */}
                <Route
                  path={ROUTER_CONST.noticeManagement}
                  render={(props) => <NoticeManagement {...props} />}
                />
                <Route
                  path={ROUTER_CONST.notice + "/:status"}
                  component={NoticeManagement}
                />

                <Route
                  path={ROUTER_CONST.citizenManagement}
                  component={CitizenManagement}
                />
                <Route
                  path={ROUTER_CONST.householdManagement}
                  component={HouseholdManagement}
                />
                <Route
                  path={ROUTER_CONST.totalManagement}
                  component={TotalManagement}
                />

                <Route
                  path={ROUTER_CONST.userManagement}
                  component={UserManagement}
                />
                <Route
                  path={ROUTER_CONST.censusManagement}
                  component={CensusManagement}
                />
                <Route
                  path={ROUTER_CONST.changePassword}
                  component={ChangPassWord}
                />

                <Route path="*" component={NotFoundPage} />
              </Switch>
            </ContentAdmin>

            <FooterCustom />
          </Layout>
        </Layout>
      </Suspense>
    </Fragment>
  );
};

export default AppRouter;
