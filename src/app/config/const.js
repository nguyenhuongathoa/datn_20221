export const ROUTER_CONST = {
  home: "/",
  news: "/news",
  newDetail: "/news/:id",
  login: "/login",
  admin: "/admin",
  administrative: "/administrative",
  police: "/police",
  create: "/create",
  notice: "/admin/notice",
  noticeManagement: "/admin/notice-management",
  householdManagement: "/admin/household-management",
  citizenManagement: "/admin/citizen-management",
  totalManagement: "/admin/total-management",
  userManagement: "/admin/user-management",
  changePassword: "/admin/changePassword",
  censusManagement: "/admin/census-management",
  householdCreateAndUpdate: "/admin/household-create-update",
  administrativeNoticeManagement: "/administrative/notice-management",
  administrativeNotice: "/administrative/notice",
  administrativeChangePassword: "/Administrative/changePassword",
  policeHouseholdManagement: "/police/household-management",
  policeCitizenManagement: "/police/citizen-management",
  policeTotalManagement: "/police/total-management",
  policeChangePassword: "/police/changePassword",
  policeCensusManagement: "/police/census-management",
};

export const PARAMS_CONST = {};

export const USER_NAME = "userName";
export const USER_ID = "userId";
export const ROLE = "role";
export const CITIZEN_ID = "citizenId";
export const FULL_NAME = "fullName";
export const ROLE_TYPE = "roleType";
export const GET_LIST_ERROR = "Lấy dữ liệu không thành công!";
export const NotificationStyle = {
  success: {
    color: "rgba(0, 0, 0, 0.65)",
    border: "1px solid #b7eb8f",
    backgroundColor: "#f6ffed",
  },
  warning: {
    color: "rgba(0, 0, 0, 0.65)",
    border: "1px solid #ffe58f",
    backgroundColor: "#fffbe6",
  },
  error: {
    color: "rgba(0, 0, 0, 0.65)",
    border: "1px solid #ffa39e",
    backgroundColor: "#fff1f0",
  },
  info: {
    color: "rgba(0, 0, 0, 0.65)",
    border: "1px solid #91d5ff",
    backgroundColor: "#e6f7ff",
  },
};
export const statusNotice = [
  { label: "Còn hiệu lực", value: 1 },
  { label: "Hết hiệu lực", value: 0 },
];

export const statusAlive = [
  { label: "Còn sống", value: 1 },
  { label: "Đã mất", value: 0 },
];

export const statusHousehold = [
  { label: "Còn sinh sống", value: 1 },
  { label: "Đã chuyển hộ khẩu", value: 0 },
];

export const isactive = [
  { label: "Còn sinh sống", value: true },
  { label: "Đã chuyển đi", value: false },
];
