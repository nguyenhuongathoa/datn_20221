import React, { Fragment, Suspense } from "react";
import { Route, Switch } from "react-router-dom";
import NotFoundPage from "../page/error";
import { ROUTER_CONST } from "./const";
import { Layout } from "antd";
import FooterCustom from "../component/layout/footer/Footer";
import HeaderCustom from "../component/layout/header/Header";
import ContentAdmin from "../component/layout/contentAdmin";
import SidebarPolice from "../component/layout/sidebar/sidebarPolice";
import CitizenManagement from "../component/pageComponent/citizenManagement";
import HouseholdManagement from "../component/pageComponent/householdManagement";
import TotalManagement from "../component/pageComponent/totalManagement";
import ChangPassWord from "../component/pageComponent/changPassword";
import CensusManagement from "../component/pageComponent/censusManagement";

const AppRouterPolice = () => {
  const loader = <div className="example">Loading...</div>;

  return (
    <Fragment>
      <Suspense fallback={loader}>
        <Layout style={{ minHeight: "100vh" }}>
          <SidebarPolice />
          <Layout className="site-layout">
            <HeaderCustom />
            <ContentAdmin>
              <Switch>
                <Route
                  path={ROUTER_CONST.policeHouseholdManagement}
                  component={HouseholdManagement}
                />
                <Route
                  path={ROUTER_CONST.policeCitizenManagement}
                  component={CitizenManagement}
                />
                <Route
                  path={ROUTER_CONST.policeTotalManagement}
                  component={TotalManagement}
                />
                <Route
                  path={ROUTER_CONST.policeCensusManagement}
                  component={CensusManagement}
                />
                <Route
                  path={ROUTER_CONST.policeChangePassword}
                  component={ChangPassWord}
                />
                <Route path="*" component={NotFoundPage} />
              </Switch>
            </ContentAdmin>

            <FooterCustom />
          </Layout>
        </Layout>
      </Suspense>
    </Fragment>
  );
};

export default AppRouterPolice;
