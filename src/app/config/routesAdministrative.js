import React, { Fragment, Suspense } from "react";
import { Route, Switch } from "react-router-dom";
import NotFoundPage from "../page/error";
import { ROUTER_CONST } from "./const";
import { Layout } from "antd";
import FooterCustom from "../component/layout/footer/Footer";
import HeaderCustom from "../component/layout/header/Header";
import ContentAdmin from "../component/layout/contentAdmin";
import NoticeManagement from "../component/pageComponent/noticeManagement";
import SidebarAdministrative from "../component/layout/sidebar/SidebarAdministrative";
import ChangPassWord from "../component/pageComponent/changPassword";

const AppRouterAdministrative = () => {
  const loader = <div className="example">Loading...</div>;

  return (
    <Fragment>
      <Suspense fallback={loader}>
        <Layout style={{ minHeight: "100vh" }}>
          <SidebarAdministrative />
          <Layout className="site-layout">
            <HeaderCustom />
            <ContentAdmin>
              <Switch>
                <Route
                  path={ROUTER_CONST.administrativeNoticeManagement}
                  render={(props) => <NoticeManagement {...props} />}
                />
                <Route
                  path={ROUTER_CONST.administrativeNotice + "/:status"}
                  component={NoticeManagement}
                />
                <Route
                  path={ROUTER_CONST.administrativeChangePassword}
                  component={ChangPassWord}
                />
                <Route path="*" component={NotFoundPage} />
              </Switch>
            </ContentAdmin>

            <FooterCustom />
          </Layout>
        </Layout>
      </Suspense>
    </Fragment>
  );
};

export default AppRouterAdministrative;
