const baseUrl = process.env.REACT_APP_API_URL;

const API = {
  news: {
    getList: baseUrl + "/v1/notice",
    create: "/v1/notice",
  },
  citizen: {
    getList: baseUrl + "/v1/citizen/search",
    create: "/v1/citizen/create",
    update: "/v1/citizen/update",
    delete: "v1/citizen/delete/",
    getForHost: "/v1/citizen/list-for-host/",
    getForUser: "/v1/citizen/list-for-user",
  },
  household: {
    getList: baseUrl + "/v1/house-hold/search",
    create: "/v1/house-hold/create",
    update: "/v1/house-hold/update",
    delete: "/v1/house-hold/delete/",
    getID: "/v1/house-hold/",
  },
  reference: {
    getCategory: baseUrl + "/v1/reference?type=",
  },
  total: {
    sex: baseUrl + "/v1/total/sex",
    vaccin: baseUrl + "/v1/total/vaccin",
    age: baseUrl + "/v1/total/age",
    nation: baseUrl + "/v1/total/nation",
    education: baseUrl + "/v1/total/education",
    religion: baseUrl + "/v1/total/religion",
  },
  user: {
    getList: baseUrl + "/v1/user",
    delete: "/v1/user/delete/",
    create: "/v1/user/create",
    changePassword: "/v1/user/update-password",
  },
  census: {
    getAll: baseUrl + "/v1/census/getAll",
    get: "/v1/census/caculate",
    delete: "/v1/census/delete/",
    create: "/v1/census/create",
  },
};

export default API;
