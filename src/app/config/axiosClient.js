import axios from "axios";
// import { refresh } from "../../components/common/saveData/saveStore";
// import { redirectLogin } from "../../libs/redirectLogin";
// import { getToken } from "../../libs/saveAndGetData/getData";

const baseURL = `${process.env.REACT_APP_API_URL}`;

const instance = axios.create({
  baseURL,
  timeout: 300000,
  headers: {
    "Content-Type": "application/json",
  },
});

instance.interceptors.response.use(
  (res) => {
    // debugger
    const originalRequest = res.config;
    const code = res.data.code;
    return res;
    // debugger
    // if (code === 0) {
    //   return res.data;
    // } else if (code === 86 || code === 85) {
    //   return refresh()
    //     .then(() => {
    //       let data = JSON.parse(originalRequest.data);
    //       let token = getToken();
    //       data.token = token;
    //       originalRequest.data = data;
    //       originalRequest._retry = true;
    //       return instance(originalRequest);
    //     })
    //     .catch((error) => {
    //       redirectLogin();
    //     });
    // } else if (code === 83 || code === 87) {
    //   //83 SAI CHU KY, 87 CHUA DANG NHAP
    //   redirectLogin();
    // } else {
    //   console.log(res.data, "error");
    //   return res.data;
    //   // return Promise.reject(res.data.message);
    // }
  },
  (error) => {
    console.log(error, "error out 200");
    const status = error.response;
    if (status === 401) {
      // redirectLogin();
    }
    return Promise.reject(error);
  }
);

instance.interceptors.request.use((config) => {
  return config;
});

export default instance;
